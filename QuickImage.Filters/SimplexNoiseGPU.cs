﻿using System;
using System.Drawing;
using System.Linq;
using QuickImage.Core;
using QuickImage.Core.OpenGL;

namespace QuickImage.Filters
{
	public static class SimplexNoiseGPU
	{
		public static void Run(RasterBuffer<Color> buffer, int seed, float frequencyX, float frequencyY, float amplitude, float persistence, float time, int octaves, bool horizontallySeamless, bool verticallySeamless, ProgressListener onProgress) {
			int[] perm, permMod12;
			computePerm(seed, out perm, out permMod12);
			
			var iw = 1f / buffer.Width;
			var ih = 1f / buffer.Height;
			
			var uniforms = new UniformsCollection {
				{ "frequencyX", frequencyX },
				{ "frequencyY", frequencyY },
				{ "amplitude", amplitude },
				{ "persistence", persistence },
				{ "time", time },
				{ "octaves", octaves },
				
				{ "perm", perm },
				{ "permMod12", permMod12 },
				{ "iw", iw },
				{ "ih", ih }
			};
			
			const string code = @"
			
			vec4[] grad4 = vec4[] (
				vec4(0,1,1,1), vec4(0,1,1,-1), vec4(0,1,-1,1), vec4(0,1,-1,-1),
				vec4(0,-1,1,1), vec4(0,-1,1,-1), vec4(0,-1,-1,1), vec4(0,-1,-1,-1),
				vec4(1,0,1,1), vec4(1,0,1,-1), vec4(1,0,-1,1), vec4(1,0,-1,-1),
				vec4(-1,0,1,1), vec4(-1,0,1,-1), vec4(-1,0,-1,1), vec4(-1,0,-1,-1),
				vec4(1,1,0,1), vec4(1,1,0,-1), vec4(1,-1,0,1), vec4(1,-1,0,-1),
				vec4(-1,1,0,1), vec4(-1,1,0,-1), vec4(-1,-1,0,1), vec4(-1,-1,0,-1),
				vec4(1,1,1,0), vec4(1,1,-1,0), vec4(1,-1,1,0), vec4(1,-1,-1,0),
				vec4(-1,1,1,0), vec4(-1,1,-1,0), vec4(-1,-1,1,0), vec4(-1,-1,-1,0)
			);
			
			float F4 = 0.30901699437494742410229341718282;  //(sqrt(5.0)-1.0)/4.0;
			float G4 = 0.13819660112501051517954131656344;  //(5.0-sqrt(5.0))/20.0;
			
			// 4D simplex noise, better simplex rank ordering method 2012-03-09
			float noise(float x, float y, float z, float w) {
				float n0, n1, n2, n3, n4; // Noise contributions from the five corners
				// Skew the (x,y,z,w) space to determine which cell of 24 simplices we're in
				float s = (x + y + z + w) * F4; // Factor for 4D skewing
				int i = int(floor(x + s));
				int j = int(floor(y + s));
				int k = int(floor(z + s));
				int l = int(floor(w + s));
				float t = (i + j + k + l) * G4; // Factor for 4D unskewing
				float X0 = i - t; // Unskew the cell origin back to (x,y,z,w) space
				float Y0 = j - t;
				float Z0 = k - t;
				float W0 = l - t;
				float x0 = x - X0;  // The x,y,z,w distances from the cell origin
				float y0 = y - Y0;
				float z0 = z - Z0;
				float w0 = w - W0;
				// For the 4D case, the simplex is a 4D shape I won't even try to describe.
				// To find out which of the 24 possible simplices we're in, we need to
				// determine the magnitude ordering of x0, y0, z0 and w0.
				// Six pair-wise comparisons are performed between each possible pair
				// of the four coordinates, and the results are used to rank the numbers.
				int rankx = 0;
				int ranky = 0;
				int rankz = 0;
				int rankw = 0;
				if(x0 > y0) rankx++; else ranky++;
				if(x0 > z0) rankx++; else rankz++;
				if(x0 > w0) rankx++; else rankw++;
				if(y0 > z0) ranky++; else rankz++;
				if(y0 > w0) ranky++; else rankw++;
				if(z0 > w0) rankz++; else rankw++;
				int i1, j1, k1, l1; // The integer offsets for the second simplex corner
				int i2, j2, k2, l2; // The integer offsets for the third simplex corner
				int i3, j3, k3, l3; // The integer offsets for the fourth simplex corner
				// simplex[c] is a 4-vector with the numbers 0, 1, 2 and 3 in some order.
				// Many values of c will never occur, since e.g. x>y>z>w makes x<z, y<w and x<w
				// impossible. Only the 24 indices which have non-zero entries make any sense.
				// We use a thresholding to set the coordinates in turn from the largest magnitude.
				// Rank 3 denotes the largest coordinate.
				i1 = rankx >= 3 ? 1 : 0;
				j1 = ranky >= 3 ? 1 : 0;
				k1 = rankz >= 3 ? 1 : 0;
				l1 = rankw >= 3 ? 1 : 0;
				// Rank 2 denotes the second largest coordinate.
				i2 = rankx >= 2 ? 1 : 0;
				j2 = ranky >= 2 ? 1 : 0;
				k2 = rankz >= 2 ? 1 : 0;
				l2 = rankw >= 2 ? 1 : 0;
				// Rank 1 denotes the second smallest coordinate.
				i3 = rankx >= 1 ? 1 : 0;
				j3 = ranky >= 1 ? 1 : 0;
				k3 = rankz >= 1 ? 1 : 0;
				l3 = rankw >= 1 ? 1 : 0;
				// The fifth corner has all coordinate offsets = 1, so no need to compute that.
				float x1 = x0 - i1 + G4; // Offsets for second corner in (x,y,z,w) coords
				float y1 = y0 - j1 + G4;
				float z1 = z0 - k1 + G4;
				float w1 = w0 - l1 + G4;
				float x2 = x0 - i2 + 2.0*G4; // Offsets for third corner in (x,y,z,w) coords
				float y2 = y0 - j2 + 2.0*G4;
				float z2 = z0 - k2 + 2.0*G4;
				float w2 = w0 - l2 + 2.0*G4;
				float x3 = x0 - i3 + 3.0*G4; // Offsets for fourth corner in (x,y,z,w) coords
				float y3 = y0 - j3 + 3.0*G4;
				float z3 = z0 - k3 + 3.0*G4;
				float w3 = w0 - l3 + 3.0*G4;
				float x4 = x0 - 1.0 + 4.0*G4; // Offsets for last corner in (x,y,z,w) coords
				float y4 = y0 - 1.0 + 4.0*G4;
				float z4 = z0 - 1.0 + 4.0*G4;
				float w4 = w0 - 1.0 + 4.0*G4;
				// Work out the hashed gradient indices of the five simplex corners
				int ii = i & 255;
				int jj = j & 255;
				int kk = k & 255;
				int ll = l & 255;
				int gi0 = perm[ii+perm[jj+perm[kk+perm[ll]]]] % 32;
				int gi1 = perm[ii+i1+perm[jj+j1+perm[kk+k1+perm[ll+l1]]]] % 32;
				int gi2 = perm[ii+i2+perm[jj+j2+perm[kk+k2+perm[ll+l2]]]] % 32;
				int gi3 = perm[ii+i3+perm[jj+j3+perm[kk+k3+perm[ll+l3]]]] % 32;
				int gi4 = perm[ii+1+perm[jj+1+perm[kk+1+perm[ll+1]]]] % 32;
				// Calculate the contribution from the five corners
				float t0 = 0.6 - x0*x0 - y0*y0 - z0*z0 - w0*w0;
				if(t0<0) n0 = 0.0;
				else {
					t0 *= t0;
					n0 = t0 * t0 * dot(grad4[gi0], vec4(x0, y0, z0, w0));
				}
			 float t1 = 0.6 - x1*x1 - y1*y1 - z1*z1 - w1*w1;
				if(t1<0) n1 = 0.0;
				else {
					t1 *= t1;
					n1 = t1 * t1 * dot(grad4[gi1], vec4(x1, y1, z1, w1));
				}
			 float t2 = 0.6 - x2*x2 - y2*y2 - z2*z2 - w2*w2;
				if(t2<0) n2 = 0.0;
				else {
					t2 *= t2;
					n2 = t2 * t2 * dot(grad4[gi2], vec4(x2, y2, z2, w2));
				}
			 float t3 = 0.6 - x3*x3 - y3*y3 - z3*z3 - w3*w3;
				if(t3<0) n3 = 0.0;
				else {
					t3 *= t3;
					n3 = t3 * t3 * dot(grad4[gi3], vec4(x3, y3, z3, w3));
				}
			 float t4 = 0.6 - x4*x4 - y4*y4 - z4*z4 - w4*w4;
				if(t4<0) n4 = 0.0;
				else {
					t4 *= t4;
					n4 = t4 * t4 * dot(grad4[gi4], vec4(x4, y4, z4, w4));
				}
				// Sum up and scale the result to cover the range [-1,1]
				return 27.0 * (n0 + n1 + n2 + n3 + n4);
			}
			
			float pi = 3.1415926535897932384626433832795;
			float twoPi = 2 * pi;
			float oneOverTwoPi = 1 / twoPi;
			
			float seamlessNoise(float x, float y, float dx, float dy, float offset) {
				float nx = offset + cos(x * twoPi) * dx * oneOverTwoPi;
				float ny = offset + cos(y * twoPi) * dy * oneOverTwoPi;
				float nz = offset + sin(x * twoPi) * dx * oneOverTwoPi;
				float nw = offset + sin(y * twoPi) * dy * oneOverTwoPi;
				
				return noise(nx, ny, nz, nw);
			}
			
			vec4 compute(int x, int y) {
				float max = 0;
				float val = 0;
				float multiplier = 1;
				float pow = 1;
				for (int octave = 0; octave < octaves; octave++) {
					val += seamlessNoise(x * iw, y * ih, frequencyX * pow, frequencyY * pow, time) * multiplier;
					max += multiplier;
					multiplier *= persistence;
					pow *= 2;
				}
				
				val = clamp(val * amplitude / max, -1, +1);  // [-1, +1]
				val += 1;  // [0, +2]
				val *= .5;  // [0, +1]
				
				return vec4(val, val, val, 1);
			}
			";
			
			Renderer.Render(buffer, uniforms, code, onProgress);
		}
		
		static void computePerm(int seed, out int[] perm, out int[] permMod12) {
			var rand = new Random(seed);
			
			perm = new int[512];
			permMod12 = new int[512];
			
			for (int i = 0; i < 256; i++)
				perm[i] = i;
			
			shuffle(perm, 0, 256, rand);  // Shuffle indices in the first half.
			
			for (int i = 0; i < 256; i++)
				permMod12[i] = (perm[i] % 12);
			
			Array.Copy(perm, 0, perm, 256, 256);  // Duplicate first half of the permutation table to the second half.
			Array.Copy(permMod12, 0, permMod12, 256, 256);
		}
		
		static void shuffle<T>(T[] array, int index, int count, Random random) {
			for(int i = index; i < count; i++)
				swap(array, i, random.Next(index, index+count));
		}
		
		static void swap<T>(T[] array, int index1, int index2) {
			T buffer = array[index1];
			array[index1] = array[index2];
			array[index2] = buffer;
		}
	}
}
