﻿using System;
using System.Drawing;
using QuickImage.Core;

namespace QuickImage.Filters
{
	public static class Clear
	{
		/// <summary>Clears the content of <paramref name="buffer"/>.</summary>
		/// <param name="buffer">The buffer to be cleared.</param>
		/// <param name="color">The color to fill the buffer with.</param>
		public static void Run(RasterBuffer<Color> buffer, Color color) {
			for (var x = 0; x < buffer.Width; x++)
				for (var y = 0; y < buffer.Height; y++)
					buffer[x, y] = color;
		}
	}
}
