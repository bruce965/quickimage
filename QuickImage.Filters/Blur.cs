﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Tools;

namespace QuickImage.Filters
{
	/// <summary>Blurs an image with a fast and simple filter.</summary>
	public static class Blur
	{
		/// <summary>Blurs <paramref name="source"/> with a fast and simple filter.</summary>
		/// <param name="source">The buffer to be blurred.</param>
		/// <param name="target">The buffer to put blurred <paramref name="source"/> in, can coincide with <paramref name="source"/>.</param>
		/// <param name="radius">The blur radius in pixels.</param>
		/// <param name="horizontallySeamless">Resulting image results tileable horizontally if <c>true</c>.</param>
		/// <param name="verticallySeamless">Resulting image results tileable vertically if <c>true</c>.</param>
		/// <param name="onProgress">Listen progress as this filter is applied.</param>
		public static void Run(RasterBuffer<Color> source, RasterBuffer<Color> target, int radius, bool horizontallySeamless, bool verticallySeamless, ProgressListener onProgress) {
			if (source.Size != target.Size)
				throw new ArgumentException("Source and target must be of the same size");
			
			if (source == target)
				source = source.Clone();
			
			for (var y = 0; y < target.Height; y++)
			for (var x = 0; x < target.Width; x++) {
				var avgA = 0;
				var avgR = 0;
				var avgG = 0;
				var avgB = 0;
				
				var total = 0;
				var pixels = 0;
				
				for (var yb = y - radius; yb < y + radius; yb++)
				for (var xb = x - radius; xb < x + radius; xb++) {
					var px = source.GetPixelSeamless(xb, yb, horizontallySeamless, verticallySeamless);
					
					avgA += px.A;
					total++;
					
					if (px.A != 0) {
						avgR += px.R;
						avgG += px.G;
						avgB += px.B;
						
						pixels++;
					}
				}
				
				if (pixels != 0) {
					var oneOverPixels = 1f / pixels;
					var oneOverTotal = 1f / total;
					avgA = (int)(avgA * oneOverTotal);
					avgR = (int)(avgR * oneOverPixels);
					avgG = (int)(avgG * oneOverPixels);
					avgB = (int)(avgB * oneOverPixels);
					
					target[x, y] = new FColor(avgA, avgR, avgG, avgB);
				}
				
				if (onProgress != null)
					onProgress((float)y / target.Height);
			}
		}
	}
}
