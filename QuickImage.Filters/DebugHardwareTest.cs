﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Core.OpenGL;
using QuickImage.Core.OpenGL.GLSL;

namespace QuickImage.Filters
{
	public static class DebugHardwareTest
	{
		public static void Run(RasterBuffer<Color> buffer, ProgressListener onProgress) {
			var color1 = new vec4(1, 0, 0, 1);
			var color2 = new vec4(0, 0, 1, 0.5f);
			
			var uniforms = new UniformsCollection {
				{ "color1", color1 },
				{ "color2", color2 }
			};
			
			const string code = @"
				vec4 compute(int x, int y) {
					return x > 0 ? color1 : color2;
				}
			";
			
			Renderer.Render(buffer, uniforms, code, onProgress);
		}
	}
}
