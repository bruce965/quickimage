﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Tools;

namespace QuickImage.Filters
{
	/// <summary>Rotates an image by a right angle.</summary>
	public static class Rotate
	{
		/// <summary>Rotates <paramref name="buffer"/> by <paramref name="angle"/>.</summary>
		/// <param name="buffer">The buffer to be rotated.</param>
		/// <param name="angle">A right angle to rotate <paramref name="buffer"/> by.</param>
		public static void Run(RasterBuffer<Color> buffer, int angle) {
			angle %= 360;
			if (angle < 0)
				angle += 360;
			
			if (angle % 90 != 0)
				throw new ArgumentException("Only multiples of 90 are valid angles for this filter");
			
			if (angle == 90 || angle == 270) {
				buffer.X = buffer.X + (buffer.Width - buffer.Height) / 2;
				buffer.Y = buffer.Y + (buffer.Height - buffer.Width) / 2;
			}
			
			// TODO: do not use `ToBitmap`.
			var bitmap = buffer.ToBitmap();
			
			switch (angle) {
			case 90:
				bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
				break;
			case 180:
				bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
				break;
			case 270:
				bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
				break;
			}
			
			buffer.ImportImage(bitmap);
		}
	}
}
