﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Tools;
using QuickImage.Filters.Internal;

namespace QuickImage.Filters
{
	/// <summary>Produces an effect of melting an image downwards.</summary>
	public static class Slur
	{
		/// <summary>Produces an effect of melting <paramref name="source"/> downwards.</summary>
		/// <param name="source">The buffer to be slurred.</param>
		/// <param name="target">The buffer to put slurred <paramref name="source"/> in, can coincide with <paramref name="source"/>.</param>
		/// <param name="seed">A seed to be used to generate slur.</param>
		/// <param name="randomization">Probability of a pixel to be slurred (between <c>0</c> and <c>1</c>).</param>
		/// <param name="repetitions">Number of repetitions for slur.</param>
		/// <param name="horizontallySeamless">Resulting image results tileable horizontally if <c>true</c>.</param>
		/// <param name="verticallySeamless">Resulting image results tileable vertically if <c>true</c>.</param>
		/// <param name="onProgress">Listen progress as this filter is applied.</param>
		public static void Run(RasterBuffer<Color> source, RasterBuffer<Color> target, int seed, float randomization, int repetitions, bool horizontallySeamless, bool verticallySeamless, ProgressListener onProgress) {
			if (source.Size != target.Size)
				throw new ArgumentException("Source and target must be of the same size");
			
			var width = source.Width;
			var height = source.Height;
			
			var rand = new Random(seed);
			for (var i = 0; i < repetitions; i++) {
				// from bottom to top else we'd read already altered pixels if source == target
				for (var y = height - 1; y >= 0; y--) {
					for (var x = 0; x < width; x++) {
						if (rand.NextDouble() < randomization) {
							var color = source.GetPixelSeamless(x + (rand.NextDouble() < 0.8 ? 0 : rand.Next() & 1), y - 1, horizontallySeamless, verticallySeamless);
							target[x, y] = color;
						}
					}
					
					if (onProgress != null)
						onProgress((float)(i * height + height - y - 1) / (repetitions * target.Height));
				}
			}
		}
	}
}
