﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Tools;
using QuickImage.Filters.Internal;

namespace QuickImage.Filters
{
	/// <summary>Applies an additive or speckle noise to an image.</summary>
	public static class RGBNoise
	{
		/// <summary>Applies an additive or speckle noise to <paramref name="buffer"/>.</summary>
		/// <param name="buffer">The buffer to apply the noise to.</param>
		/// <param name="seed">A seed to be used to generate noise.</param>
		/// <param name="correlated">Multiplicative (speckle) noise if <c>true</c>, additive noise otherwise.</param>
		/// <param name="independent">Color parameters define hue of the noise if <c>true</c>, they define max noise for each channel otherwise.</param>
		/// <param name="red">Multiplier for red channel.</param>
		/// <param name="green">Multiplier for green channel.</param>
		/// <param name="blue">Multiplier for blue channel.</param>
		/// <param name="alpha">Multiplier for alpha channel.</param>
		public static void Run(RasterBuffer<Color> buffer, int seed, bool correlated, bool independent, float red, float green, float blue, float alpha) {
			var width = buffer.Width;
			var height = buffer.Height;
			
			var rand = new Random(seed);
			for (var y = 0; y < height; y++) {
				for (var x = 0; x < width; x++) {
					var background = buffer[x, y];
					var foreground = makePixel(rand, background, correlated, independent, red, green, blue, alpha);
					buffer[x, y] = background.AlphaBlend(foreground);
				}
			}
		}
		
		static Color makePixel(Random rand, Color baseColor, bool correlated, bool independent, float red, float green, float blue, float alpha) {
			var av = (float)rand.NextDouble();
			var rv = av;
			var gv = av;
			var bv = av;
			
			if (!independent) {
				rv = (float)rand.NextDouble();
				gv = (float)rand.NextDouble();
				bv = (float)rand.NextDouble();
			}
			
			var a = alpha;
			var r = red;
			var g = green;
			var b = blue;
			
			if (correlated) {
				a *= baseColor.A * (1f / 255f);
				r *= baseColor.R * (1f / 255f);
				g *= baseColor.G * (1f / 255f);
				b *= baseColor.B * (1f / 255f);
			}
			
			return new FColor(av * a, rv * r, gv * g, bv * b);
		}
	}
}
