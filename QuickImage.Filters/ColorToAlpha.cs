﻿using System;
using System.Drawing;
using QuickImage.Core;

namespace QuickImage.Filters
{
	public static class ColorToAlpha
	{
		public static void Run(RasterBuffer<Color> source, RasterBuffer<Color> target, Color color, ProgressListener onProgress) {
			if (source.Size != target.Size)
				throw new ArgumentException("Source and target must be of the same size");
			
			var width = source.Width;
			var height = source.Height;
			
			for (var y = 0; y < height; y++) {
				for (var x = 0; x < width; x++) {
					var icolor = source[x, y];
					var ocolor = makePixel(icolor, color);
					target[x, y] = ocolor;
				}
				
				if (onProgress != null)
					onProgress((float)y / target.Height);
			}
		}
		
		static Color makePixel(Color pixel, Color color) {
			var alpha = max(
				pixel.R > color.R ? 255 * (pixel.R - color.R) / (255 - color.R) : pixel.R < color.R ? 255 * (color.R - pixel.R) / (color.R) : 0,
				pixel.G > color.G ? 255 * (pixel.G - color.G) / (255 - color.G) : pixel.G < color.G ? 255 * (color.G - pixel.G) / (color.G) : 0,
				pixel.B > color.B ? 255 * (pixel.B - color.B) / (255 - color.B) : pixel.B < color.B ? 255 * (color.B - pixel.B) / (color.B) : 0
			);
			
			if (alpha == 0)
				return Color.FromArgb(alpha, pixel.R, pixel.G, pixel.B);
			
			return Color.FromArgb(
				alpha * pixel.A / 255,
				(byte)(255 * (pixel.R - color.R) / alpha + color.R),
				(byte)(255 * (pixel.G - color.G) / alpha + color.G),
				(byte)(255 * (pixel.B - color.B) / alpha + color.B)
			);
		}
		
		static int max(int v1, int v2, int v3) {
			return Math.Max(v1, Math.Max(v2, v3));
		}
	}
}
