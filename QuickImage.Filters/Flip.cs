﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Tools;

namespace QuickImage.Filters
{
	/// <summary>Flips an image.</summary>
	public static class Flip
	{
		/// <summary>Flips <paramref name="buffer"/>.</summary>
		/// <param name="buffer">The buffer to be flipped.</param>
		/// <param name="flipHorizontally"><paramref name="buffer"/> will be flipped horizontally.</param>
		/// <param name="flipVertically"><paramref name="buffer"/> will be flipped vertically.</param>
		public static void Run(RasterBuffer<Color> buffer, bool flipHorizontally, bool flipVertically) {
			// TODO: do not use `ToBitmap`.
			using (var bitmap = buffer.ToBitmap()) {
				bitmap.RotateFlip(
					flipHorizontally ?
						flipVertically ?
							RotateFlipType.RotateNoneFlipXY :
							RotateFlipType.RotateNoneFlipX :
						flipVertically ?
							RotateFlipType.RotateNoneFlipY :
							RotateFlipType.RotateNoneFlipNone
				);
				
				// TODO: also update offset.
				
				buffer.ImportImage(bitmap);
			}
		}
	}
}
