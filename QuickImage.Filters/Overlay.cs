﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Tools;

namespace QuickImage.Filters
{
	/// <summary>Overlays an image over another.</summary>
	public static class Overlay
	{
		/// <summary>Overlays <paramref name="overlay"/> over <paramref name="buffer"/>.</summary>
		/// <param name="buffer">The background buffer.</param>
		/// <param name="overlay">The buffer to overlay over <paramref name="buffer"/>.</param>
		/// <param name="onProgress">Listen progress as this filter is applied.</param>
		public static void Run(RasterBuffer<Color> buffer, RasterBuffer<Color> overlay, ProgressListener onProgress) {
			var offsetx = overlay.X - buffer.X;
			var offsety = overlay.Y - buffer.Y;
			
			for (var y = 0; y < overlay.Height; y++) {
				var dy = y + offsety;
				
				if (dy < 0)
					continue;  // TODO: optimize.
				
				if (dy >= buffer.Height)
					break;
				
				for (var x = 0; x < overlay.Width; x++) {
					var dx = x + offsetx;
					
					if (dx < 0)
						continue;  // TODO: optimize.
					
					if (dx >= buffer.Width)
						break;
					
					buffer[dx, dy] = buffer[dx, dy].AlphaBlend(overlay[x, y]);
				}
				
				if (onProgress != null)
					onProgress((float)y / overlay.Height);
			}
		}
	}
}
