﻿using System;
using System.Drawing;
using System.Linq;
using QuickImage.Core;
using QuickImage.Filters.Internal;

namespace QuickImage.Filters
{
	public static class WorleyNoise
	{
		public static void Run(RasterBuffer<Color> buffer, ProgressListener onProgress) {
			var iw = 1f / buffer.Width;
			var ih = 1f / buffer.Height;
			
			var noise = new Internal.WorleyNoise {
				intensity = 1,
				size = 10,
				offset = 0
			};
			
			for (var y = 0; y < buffer.Height; y++) {
				for (var x = 0; x < buffer.Width; x++)
					buffer[x, y] = makePixel(noise, x * iw, y * ih, 1f, 1f, 1);
				
				if (onProgress != null)
					onProgress((float)y / buffer.Height);
			}
		}
		
		static Color makePixel(Internal.WorleyNoise noise, float x, float y, float amplitude, float persistence, int octaves) {
			float max = 0;
			float val = 0;
			float multiplier = 1;
			float pow = 1;
			for (var octave = 0; octave < octaves; octave++) {
				val += noise.Compute(x, y) * multiplier;
				max += multiplier;
				multiplier *= persistence;
				pow *= 2f;
			}
			
			val = clamp(val * amplitude / max, -1, +1);  // [-1, +1]
			val += 1f;  // [0, +2]
			val *= .5f;  // [0, +1]
			
			return new FColor(val, val, val);
		}
		
		static float clamp(float v, float min, float max) {
			return Math.Min(Math.Max(v, min), max);
		}
	}
}
