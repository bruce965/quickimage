﻿using System;
using System.Drawing;
using System.Linq;
using QuickImage.Core;
using QuickImage.Filters.Internal;

namespace QuickImage.Filters
{
	public static class SimplexNoise
	{
		public static void Run(RasterBuffer<Color> buffer, int seed, float frequencyX, float frequencyY, float amplitude, float persistence, float time, int octaves, bool horizontallySeamless, bool verticallySeamless, ProgressListener onProgress) {
			var noise = Enumerable.Range(0, octaves)
				.Select(i => new Internal.SimplexNoise(seed + i))
				.ToArray();
			
			var iw = 1f / buffer.Width;
			var ih = 1f / buffer.Height;
			
			for (var y = 0; y < buffer.Height; y++) {
				for (var x = 0; x < buffer.Width; x++) {
					buffer[x, y] = makePixel(noise, x * iw, y * ih, frequencyX, frequencyY, amplitude, persistence, time, octaves);
					
					if (onProgress != null)
						onProgress((float)y / buffer.Height);
				}
			}
		}
		
		static Color makePixel(Internal.SimplexNoise[] noise, float x, float y, float frequencyX, float frequencyY, float amplitude, float persistence, float time, int octaves) {
			float max = 0;
			float val = 0;
			float multiplier = 1;
			float pow = 1;
			for (var octave = 0; octave < octaves; octave++) {
				val += seamlessNoise(noise[octave], x, y, frequencyX * pow, frequencyY * pow, time) * multiplier;
				max += multiplier;
				multiplier *= persistence;
				pow *= 2f;
			}
			
			val = clamp(val * amplitude / max, -1, +1);  // [-1, +1]
			val += 1f;  // [0, +2]
			val *= .5f;  // [0, +1]
			
			return new FColor(val, val, val);
		}
		
		static float clamp(float v, float min, float max) {
			return Math.Min(Math.Max(v, min), max);
		}
		
		const float twoPi = (float)(2d * Math.PI);
		const float oneOverTwoPi = (float)(1d / twoPi);
		
		// http://wiki.unity3d.com/index.php/Tileable_Noise
		// x and y are in [0..1] range
		// dx and dy are scale factors
		static float seamlessNoise(Internal.SimplexNoise noise, float x, float y, float dx, float dy, float offset) {
			var nx = offset + (float)Math.Cos(x * twoPi) * dx * oneOverTwoPi;
			var ny = offset + (float)Math.Cos(y * twoPi) * dy * oneOverTwoPi;
			var nz = offset + (float)Math.Sin(x * twoPi) * dx * oneOverTwoPi;
			var nw = offset + (float)Math.Sin(y * twoPi) * dy * oneOverTwoPi;
			
			return noise.Compute(nx, ny, nz, nw);
		}
	}
}
