﻿using System;
using System.Numerics;

namespace QuickImage.Filters.Internal
{
	class WorleyNoise
	{
		// http://www.willrmiller.com/?p=52
		
		public float intensity;
		//sampler2D _Random;
		public float size;
		public float offset;
		
		Vector3 getCell3D(int x, int y, int z) {
			//float u = (x + y * 31) / 20.0f;
			//float v = (z - x * 3) / 30.0f;
			//return tex2D(_Random, new Vector2(u, v));
			var rand = new Random(x + y * 31 + z * 3);
			return new Vector3((float)rand.NextDouble(), (float)rand.NextDouble(), (float)rand.NextDouble());
		}

		Vector2 cellNoise3D(Vector3 xyz) {
			int xi = (int)Math.Floor(xyz.X);
			int yi = (int)Math.Floor(xyz.Y);
			int zi = (int)Math.Floor(xyz.Z);

			float xf = xyz.X - xi;
			float yf = xyz.Y - yi;
			float zf = xyz.Z - zi;

			float dist1 = Single.PositiveInfinity;
			float dist2 = Single.PositiveInfinity;
			Vector3 cell;

			for (int z = -1; z <= 1; z++) {
				for (int y = -1; y <= 1; y++) {
					for (int x = -1; x <= 1; x++) {
						cell = getCell3D(xi + x, yi + y, zi + z);
						cell.X += x - xf;
						cell.Y += y - yf;
						cell.Z += z - zf;
						float dist = Vector3.Dot(cell, cell);
						if (dist < dist1) {
							dist2 = dist1;
							dist1 = dist;
						}
						else if (dist < dist2) {
							dist2 = dist;
						}
					}
				}
			}

			return new Vector2((float)Math.Sqrt(dist1), (float)Math.Sqrt(dist2));
		}
		
		Vector2 frag(Vector3 noisepos) {
			Vector2 dists = cellNoise3D((noisepos + new Vector3(offset)) * size);
			//Vector4 c = ((_Color * dists.X) + (_SecondaryColor * dists.Y)) * _Intensity; // Add the terms for a different look
			//Vector4 c = ((_Color * dists.X) * (_SecondaryColor * dists.Y)) * _Intensity;
			//return c;
			return dists * intensity;
		}
		
		public float Compute(float x, float y, float z) {
			return frag(new Vector3(x, y, z)).X;
		}
		
		public float Compute(float x, float y) {
			return Compute(x, y, 0);
		}
	}
}
