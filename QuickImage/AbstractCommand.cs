﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace QuickImage
{
	public abstract class AbstractCommand : ICommand
	{
		public IEnumerable<string> ParameterKeys { get; private set; }
		public int RequiredParametersCount { get; private set; }
		public int OptionalParametersCount { get; private set; }
		
		protected AbstractCommand(int paramsCount) : this(paramsCount, paramsCount) { }
		
		protected AbstractCommand(int requiredParams, int optionalParams) : this(requiredParams, optionalParams, null) { }
		
		protected AbstractCommand(params string[] paramKeys) : this(0, paramKeys) { }
		
		protected AbstractCommand(int paramsCount, params string[] paramKeys) : this(paramsCount, paramsCount, paramKeys) { }
		
		protected AbstractCommand(int requiredParams, int optionalParams, params string[] paramKeys) {
			ParameterKeys = paramKeys == null ? new string[0] : paramKeys.ToArray();
			RequiredParametersCount = requiredParams;
			OptionalParametersCount = optionalParams;
		}
		
		public abstract void Run(IDictionary<string, string> options, IList<string> args, Action<float> onProgress);
		
		protected static Bitmap OpenImageCopy(string path) {
			using (var input = Image.FromFile(path)) {
				var bitmap = new Bitmap(input.Width, input.Height, PixelFormat.Format32bppArgb);
				
				using (var graphics = Graphics.FromImage(bitmap))
					graphics.DrawImageUnscaled(input, 0, 0);
				
				return bitmap;
			}
		}
		
		protected static string Argument(IList<string> args, int index, string fallback = null) {
			if (index >= args.Count)
				return fallback;
			
			return args[index];
		}
		
		protected static string Option(IDictionary<string, string> options, string key, string fallback = null) {
			string value;
			if (!options.TryGetValue(key, out value))
				return fallback;
			return value;
		}
	}
}
