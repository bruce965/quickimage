﻿using System;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(Int32))]
	public class Int32Parser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static int Parse(string value) {
			try {
				return Int32.Parse(value);
			} catch(OverflowException e) {
				throw new FormatException("Value out of range", e);
			}
		}
	}
}
