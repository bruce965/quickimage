﻿using System;
using System.IO;
using QuickImage.Parsers.Source;
using QuickImage.Scripting;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(ScriptingLanguage))]
	public class ScriptingLanguageParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static ScriptingLanguage Parse(string value) {
			value = Path.GetFileName(value);
			
			if (value.IndexOf('.') != -1)
				value = Path.GetExtension(value).Substring(1);
			
			value = value.ToLowerInvariant();
			
			switch (value) {
			case "cs":
			case "csharp":
			case "c#":
				return ScriptingLanguage.CSharp;
			}
			
			throw new FormatException("Unknown scripting language: " + value);
		}
	}
}
