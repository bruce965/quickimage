﻿using System;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(Boolean))]
	public class BooleanParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static bool Parse(string value) {
			if (value.Equals("0", StringComparison.InvariantCulture))
				return false;
			
			if (value.Equals("1", StringComparison.InvariantCulture))
				return true;
			
			return Boolean.Parse(value);
		}
	}
}
