﻿using System;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(Double))]
	public class DoubleParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static double Parse(string value) {
			var multiplier = 1d;
			if (value.EndsWith("%", StringComparison.InvariantCulture)) {
				value = value.Substring(0, value.Length - 1);
				multiplier = 0.01d;
			}
			
			try {
				return Double.Parse(value) * multiplier;
			} catch(OverflowException e) {
				throw new FormatException("Value out of range", e);
			}
		}
	}
}
