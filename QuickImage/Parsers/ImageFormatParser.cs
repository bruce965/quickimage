﻿using System;
using System.Drawing.Imaging;
using System.IO;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(ImageFormat))]
	public class ImageFormatParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static ImageFormat Parse(string value) {
			value = Path.GetFileName(value);
			
			if (value.IndexOf('.') != -1)
				value = Path.GetExtension(value).Substring(1);
			
			value = value.ToLowerInvariant();
			
			switch (value) {
			case "bmp":
				return ImageFormat.Bmp;
			case "emf":
				return ImageFormat.Emf;
			case "exif":
				return ImageFormat.Exif;
			case "gif":
				return ImageFormat.Gif;
			case "icon":
				return ImageFormat.Icon;
			case "jpeg":
			case "jpg":
				return ImageFormat.Jpeg;
			//case "memorybmp":
			//case "memory":
			//case "":
			//	return ImageFormat.MemoryBmp;
			case "png":
				return ImageFormat.Png;
			case "tiff":
			case "tif":
				return ImageFormat.Tiff;
			case "wmf":
				return ImageFormat.Wmf;
			}
			
			throw new FormatException("Unknown image format: " + value);
		}
	}
}
