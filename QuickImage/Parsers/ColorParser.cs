﻿using System;
using System.Drawing;
using System.Linq;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(Color))]
	public class ColorParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static Color Parse(string value) {
			var color = value;
			
			try {
				if (color.StartsWith("#", StringComparison.InvariantCulture))
					return parseHashColor(color);
				
				if (
					color.StartsWith("argb", StringComparison.InvariantCulture) ||
					color.StartsWith("rgb", StringComparison.InvariantCulture) ||
					color.StartsWith("rgba", StringComparison.InvariantCulture)
				) return parseRGBAColor(color);
				
				// TODO: hsv(hue, saturation, value)
				
				var c = colorByName(color);
				if (c.HasValue)
					return c.Value;
			} catch(FormatException) { }
			
			throw new FormatException("Invalid color: " + value);
		}
		
		// rgb(red, green, blue), rgba(red, green, blue, alpha), argb(alpha, red, green, blue)
		// accepts integers (0 ~ 255), floats (0.0 ~ 1.0) and percentages (0% ~ 100%)
		// ambiguous value "1" will be parsed as integer for red, green, blue and as float for alpha (same as in CSS)
		static Color parseRGBAColor(string color) {
			color = color.ToLowerInvariant();
			
			bool alphaFirst = false;
			if (color.StartsWith("argb", StringComparison.InvariantCulture)) {
				alphaFirst = true;
				color = color.Substring(4);
			} else if (color.StartsWith("rgb", StringComparison.InvariantCulture)) {
				color = color.Substring(color.StartsWith("rgba", StringComparison.InvariantCulture) ? 4 : 3);
			}
			
			color = color.Trim();
			
			if (color.StartsWith("(", StringComparison.InvariantCulture))
				color = color.Substring(1);
			
			if (color.EndsWith(")", StringComparison.InvariantCulture))
				color = color.Substring(0, color.Length - 1);
			
			var args = color.Trim().Split(',').Select(arg => arg.Trim()).ToList();
			if (args.Count < 3)
				throw new FormatException();
			
			// args = [red, green, blue, alpha?] or [alpha?, red, green, blue]
			
			if (alphaFirst) {
				if (args.Count < 4)
					throw new FormatException();
				
				args.Insert(0, args[3]);
				args.RemoveAt(3);
			}
			
			if (args.Count < 4)
				args.Add("100%");
			
			// args = [red, green, blue, alpha]
			
			return Color.FromArgb(
				(int)(parseIntOrFloatOrPercentage(args[3], false) * 255f),
				(int)(parseIntOrFloatOrPercentage(args[0], true) * 255f),
				(int)(parseIntOrFloatOrPercentage(args[1], true) * 255f),
				(int)(parseIntOrFloatOrPercentage(args[2], true) * 255f)
			);
		}
		
		// accepts integers (0 ~ 255), floats (0.0 ~ 1.0) and percentages (0% ~ 100%)
		// ambiguous value "1" will be parsed as specified by ambiguousAsInt
		// returns a 0 ~ 1 float (assuming input value is within valid range)
		static float parseIntOrFloatOrPercentage(string str, bool ambiguousAsInt) {
			str = str.Trim();
			
			if (str.Equals("1"))
				return ambiguousAsInt ? (1f / 255f) : 1f;
			
			if (str.EndsWith("%", StringComparison.InvariantCulture)) {
				var digits = str.Substring(0, str.Length - 1).Trim();
				var value = Single.Parse(digits);
				return value / 100f;
			}
			
			var value2 = Single.Parse(str);
			
			// if Floor(value2) != value2 or if contains a non-digit which is not a '+' or a '-' it is not an integer
			if (Math.Abs(value2 - Math.Floor(value2)) > 0.001f || str.Any(chr => !Char.IsDigit(chr) && chr != '+' && chr != '-'))
				return value2;
			
			// else it is probably an integer
			return value2 / 255f;
		}
		
		static Color? colorByName(string name) {
			switch (name.ToLowerInvariant()) {
			case "aliceblue": return Color.AliceBlue;
			case "antiquewhite": return Color.AntiqueWhite;
			case "aqua": return Color.Aqua;
			case "aquamarine": return Color.Aquamarine;
			case "azure": return Color.Azure;
			case "beige": return Color.Beige;
			case "bisque": return Color.Bisque;
			case "black": return Color.Black;
			case "blanchedalmond": return Color.BlanchedAlmond;
			case "blue": return Color.Blue;
			case "blueviolet": return Color.BlueViolet;
			case "brown": return Color.Brown;
			case "burlywood": return Color.BurlyWood;
			case "cadetblue": return Color.CadetBlue;
			case "chartreuse": return Color.Chartreuse;
			case "chocolate": return Color.Chocolate;
			case "coral": return Color.Coral;
			case "cornflowerblue": return Color.CornflowerBlue;
			case "cornsilk": return Color.Cornsilk;
			case "crimson": return Color.Crimson;
			case "cyan": return Color.Cyan;
			case "darkblue": return Color.DarkBlue;
			case "darkcyan": return Color.DarkCyan;
			case "darkgoldenrod": return Color.DarkGoldenrod;
			case "darkgray": return Color.DarkGray;
			case "darkgreen": return Color.DarkGreen;
			case "darkkhaki": return Color.DarkKhaki;
			case "darkmagenta": return Color.DarkMagenta;
			case "darkolivegreen": return Color.DarkOliveGreen;
			case "darkorange": return Color.DarkOrange;
			case "darkorchid": return Color.DarkOrchid;
			case "darkred": return Color.DarkRed;
			case "darksalmon": return Color.DarkSalmon;
			case "darkseagreen": return Color.DarkSeaGreen;
			case "darkslateblue": return Color.DarkSlateBlue;
			case "darkslategray": return Color.DarkSlateGray;
			case "darkturquoise": return Color.DarkTurquoise;
			case "darkviolet": return Color.DarkViolet;
			case "deeppink": return Color.DeepPink;
			case "deepskyblue": return Color.DeepSkyBlue;
			case "dimgray": return Color.DimGray;
			case "dodgerblue": return Color.DodgerBlue;
			case "firebrick": return Color.Firebrick;
			case "floralWhite": return Color.FloralWhite;
			case "forestGreen": return Color.ForestGreen;
			case "fuchsia": return Color.Fuchsia;
			case "gainsboro": return Color.Gainsboro;
			case "ghostwhite": return Color.GhostWhite;
			case "gold": return Color.Gold;
			case "goldenrod": return Color.Goldenrod;
			case "gray": return Color.Gray;
			case "green": return Color.Green;
			case "greenyellow": return Color.GreenYellow;
			case "honeydew": return Color.Honeydew;
			case "hotpink": return Color.HotPink;
			case "indianred": return Color.IndianRed;
			case "indigo": return Color.Indigo;
			case "ivory": return Color.Ivory;
			case "khaki": return Color.Khaki;
			case "lavender": return Color.Lavender;
			case "lavenderblush": return Color.LavenderBlush;
			case "lawngreen": return Color.LawnGreen;
			case "lemonchiffon": return Color.LemonChiffon;
			case "lightblue": return Color.LightBlue;
			case "lightcoral": return Color.LightCoral;
			case "lightcyan": return Color.LightCyan;
			case "lightgoldenrodyellow": return Color.LightGoldenrodYellow;
			case "lightgray": return Color.LightGray;
			case "lightgreen": return Color.LightGreen;
			case "lightpink": return Color.LightPink;
			case "lightsalmon": return Color.LightSalmon;
			case "lightseagreen": return Color.LightSeaGreen;
			case "lightskyblue": return Color.LightSkyBlue;
			case "lightslategray": return Color.LightSlateGray;
			case "lightsteelblue": return Color.LightSteelBlue;
			case "lightyellow": return Color.LightYellow;
			case "lime": return Color.Lime;
			case "limegreen": return Color.LimeGreen;
			case "linen": return Color.Linen;
			case "magenta": return Color.Magenta;
			case "maroon": return Color.Maroon;
			case "mediumaquamarine": return Color.MediumAquamarine;
			case "mediumblue": return Color.MediumBlue;
			case "mediumorchid": return Color.MediumOrchid;
			case "mediumpurple": return Color.MediumPurple;
			case "mediumseagreen": return Color.MediumSeaGreen;
			case "mediumslateblue": return Color.MediumSlateBlue;
			case "mediumspringgreen": return Color.MediumSpringGreen;
			case "mediumturquoise": return Color.MediumTurquoise;
			case "mediumvioletred": return Color.MediumVioletRed;
			case "midnightblue": return Color.MidnightBlue;
			case "mintcream": return Color.MintCream;
			case "mistyrose": return Color.MistyRose;
			case "moccasin": return Color.Moccasin;
			case "navajowhite": return Color.NavajoWhite;
			case "navy": return Color.Navy;
			case "oldlace": return Color.OldLace;
			case "olive": return Color.Olive;
			case "olivedrab": return Color.OliveDrab;
			case "orange": return Color.Orange;
			case "orangered": return Color.OrangeRed;
			case "orchid": return Color.Orchid;
			case "palegoldenrod": return Color.PaleGoldenrod;
			case "palegreen": return Color.PaleGreen;
			case "paleturquoise": return Color.PaleTurquoise;
			case "palevioletred": return Color.PaleVioletRed;
			case "papayawhip": return Color.PapayaWhip;
			case "peachpuff": return Color.PeachPuff;
			case "peru": return Color.Peru;
			case "pink": return Color.Pink;
			case "plum": return Color.Plum;
			case "powderblue": return Color.PowderBlue;
			case "purple": return Color.Purple;
			case "red": return Color.Red;
			case "rosybrown": return Color.RosyBrown;
			case "royalblue": return Color.RoyalBlue;
			case "saddlebrown": return Color.SaddleBrown;
			case "salmon": return Color.Salmon;
			case "sandybrown": return Color.SandyBrown;
			case "seagreen": return Color.SeaGreen;
			case "seashell": return Color.SeaShell;
			case "sienna": return Color.Sienna;
			case "silver": return Color.Silver;
			case "skyblue": return Color.SkyBlue;
			case "slateblue": return Color.SlateBlue;
			case "slategray": return Color.SlateGray;
			case "snow": return Color.Snow;
			case "springgreen": return Color.SpringGreen;
			case "steelblue": return Color.SteelBlue;
			case "tan": return Color.Tan;
			case "teal": return Color.Teal;
			case "thistle": return Color.Thistle;
			case "tomato": return Color.Tomato;
			case "transparent": return Color.Transparent;
			case "turquoise": return Color.Turquoise;
			case "violet": return Color.Violet;
			case "wheat": return Color.Wheat;
			case "white": return Color.White;
			case "whitesmoke": return Color.WhiteSmoke;
			case "yellow": return Color.Yellow;
			case "yellowgreen": return Color.YellowGreen;
			}
			
			return null;
		}
		
		// #K, #KK, #RGB, #ARGB, #RRGGBB, #AARRGGBB
		static Color parseHashColor(string color) {
			if (color.StartsWith("#", StringComparison.InvariantCulture))
				color = color.Substring(1);
			
			if (color.Length == 1)  // K => KK
				color = color + color;
			
			if (color.Length == 2)  // KK => KKKKKK
				color = color + color + color;
			
			if (color.Length == 3)  // RGB => RRGGBB
				color = new String(new[] { color[0], color[0], color[1], color[1], color[2], color[2] });
			
			if (color.Length == 4)  // RGBA => RRGGBBAA
				color = new String(new[] { color[0], color[0], color[1], color[1], color[2], color[2], color[3], color[3] });
			
			if (color.Length == 6)  // RRGGBB => AARRGGBB
				color = "FF" + color;
			
			if (color.Length == 8)  // AARRGGBB
				return Color.FromArgb(
					parseHexPair(color.Substring(0, 2)),
					parseHexPair(color.Substring(2, 2)),
					parseHexPair(color.Substring(4, 2)),
					parseHexPair(color.Substring(6, 2))
				);
			
			throw new FormatException("Invalid hash-color: " + color + ".");
		}
		
		static int parseHexPair(string s) {
			if (s.Length != 2)
				throw new FormatException();
			
			return parseHex(s[0]) << 4 | parseHex(s[1]);
		}
		
		static int parseHex(char c) {
			if (c >= '0' && c <= '9')
				return c - '0';
			
			if (c >= 'A' && c <= 'F')
				return c - 'A' + 10;
			
			if (c >= 'a' && c <= 'f')
				return c - 'a' + 10;
			
			throw new FormatException("Invalid HEX character: " + c + ".");
		}
	}
}
