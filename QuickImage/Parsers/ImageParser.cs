﻿using System;
using System.Drawing;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(Image))]
	public class ImageParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static Image Parse(string value) {
			return Image.FromFile(value);
		}
	}
}
