﻿using System;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(String))]
	public class StringParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static string Parse(string value) {
			return value;
		}
	}
}
