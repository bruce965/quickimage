﻿using System;

namespace QuickImage.Parsers.Source
{
	[AttributeUsage(AttributeTargets.Class, Inherited=true, AllowMultiple=false)]
	class TypeParserAttribute : Attribute
	{
		internal static readonly Type[] None = new Type[0];
		
		public Type[] Supports { get; private set; }
		
		public TypeParserAttribute(params Type[] types) {
			Supports = types;
		}
	}
}
