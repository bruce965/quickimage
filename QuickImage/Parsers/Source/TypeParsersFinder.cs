﻿using System;
using System.Linq;
using System.Reflection;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers.Source
{
	public static class TypeParsersFinder
	{
		public static TypeParsersCollection FindInAssemblies(params Assembly[] assemblies) {
			var parsers = assemblies.SelectMany(assembly => assembly
				.GetTypes()
				.Where(type => type.IsDefined(typeof(TypeParserAttribute), true))
				.Where(type => typeof(TypeParser).IsAssignableFrom(type))
			).Select(type => (TypeParser)Activator.CreateInstance(type));
			
			var collection = new TypeParsersCollection();
			foreach (var parser in parsers)
				collection.Add(parser);
			
			return collection;
		}
	}
}
