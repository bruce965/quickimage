﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuickImage.Parsers.Source
{
	public abstract class TypeParser
	{
		public IEnumerable<Type> SupportedTypes {
			get { return GetSupportedTypes(GetType()); }
		}
		
		/// <exception cref="FormatException" />
		public abstract object ParseString(string value);
		
		public static IEnumerable<Type> GetSupportedTypes(Type parserType) {
			var attribute = (TypeParserAttribute)parserType.GetCustomAttributes(typeof(TypeParserAttribute), true).FirstOrDefault();
			if (attribute == null)
				return TypeParserAttribute.None;
			
			return attribute.Supports;
		}
	}
}
