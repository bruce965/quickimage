﻿using System;
using System.Collections.Generic;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers.Source
{
	public class TypeParsersCollection
	{
		readonly Dictionary<Type, TypeParser> parsers = new Dictionary<Type, TypeParser>();
		
		public void Add(TypeParser parser) {
			foreach (var type in parser.SupportedTypes)
				parsers.Add(type, parser);
		}
		
		public T Parse<T>(string value) {
			return (T)Parse(typeof(T), value);
		}
		
		public object Parse(Type type, string value) {
			TypeParser parser;
			if (!parsers.TryGetValue(type, out parser))
				throw new NotSupportedException("Unable to parse " + type.Name + " type");
			
			return parser.ParseString(value);
		}
	}
}
