﻿using System;
using QuickImage.Parsers.Source;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(Single))]
	public class SingleParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static float Parse(string value) {
			try {
				return checked((float)DoubleParser.Parse(value));
			} catch(OverflowException e) {
				throw new FormatException("Value out of range", e);
			}
		}
	}
}
