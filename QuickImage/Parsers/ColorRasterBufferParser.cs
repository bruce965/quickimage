﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Parsers.Source;
using QuickImage.Types;

namespace QuickImage.Parsers
{
	[TypeParser(typeof(RasterBuffer<Color>), typeof(FileRasterBuffer<Color>))]
	public class ColorRasterBufferParser : TypeParser
	{
		public override object ParseString(string value) {
			return Parse(value);
		}
		
		public static RasterBuffer<Color> Parse(string value) {
			return new FileRasterBuffer<Color>(value);
		}
	}
}
