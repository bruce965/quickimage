﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using QuickImage.Commands;
using QuickImage.Commands.Source;
using QuickImage.Parsers.Source;

namespace QuickImage
{
	class Program
	{
		public static void Main(string[] args) {
			var assemblies = new[] { typeof(Program).Assembly };
			var commands = CommandsFinder.FindInAssemblies(assemblies);
			
			var commandName = args.FirstOrDefault();
			if (commandName == null) {
				Console.WriteLine("Available commands:");
				foreach (var command in commands) {
					Console.WriteLine(
						"{0} {1}",
						command.Name.ToLowerInvariant(),
						String.Join(" ", command.Parameters.Select(parameter => String.Format("{0}={1}", parameter.Key.ToLowerInvariant(), parameter.Type.Name)))
					);
				}
				
				Environment.ExitCode = 5;
				return;
			}
			
			var selectedCommand = commands[commandName];
			
			if (selectedCommand == null) {
				Console.Error.WriteLine("Command \"{0}\" not found!", commandName);
				Environment.ExitCode = 1;
				return;
			}

			var parsers = TypeParsersFinder.FindInAssemblies(assemblies);
			var commandInstance = new CommandInstance(selectedCommand, parsers);
			
			var argIndex = 0;
			foreach (var arg in args.Skip(1)) {
				var divIndex = arg.IndexOf('=');
				if (divIndex != -1) {
					var key = arg.Substring(0, divIndex);
					var value = arg.Substring(divIndex + 1);
					try {
						commandInstance[key] = value;
					} catch(KeyNotFoundException) {
						Console.Error.WriteLine("Unknown parameter: \"{0}\"!", key);
					}
				} else {
					commandInstance[argIndex++] = arg;
				}
			}
			
			#if !DEBUG
			try {
			#endif
				//Console.WriteLine(
				//	"{0} {1}",
				//	commandInstance.Name,
				//	String.Join(" ", commandInstance.Parameters.Select(parameter => String.Format("{0}={1}", parameter.Key, commandInstance[parameter.Key])))
				//);
				
				reportProgress(commandInstance.Name, 0f);
				commandInstance.Execute(progress => reportProgress(commandInstance.Name, progress, true));
				reportProgress(commandInstance.Name, 1f);
				Console.WriteLine();
			#if !DEBUG
			} catch(Exception e) {
				Console.WriteLine();
				Console.Error.WriteLine("{0}", e.Message);
				Environment.ExitCode = -1;
				return;
			}
			#endif
		}
		
		static DateTime lastProgressReport = new DateTime(0);
		static void reportProgress(string command, float percentage, bool optional = false) {
			var now = DateTime.UtcNow;
			if (optional && (now - lastProgressReport).TotalMilliseconds < 100)
				return;
			
			lastProgressReport = now;
			
			var cpb = new ConsoleProgressBar { MarginLeft = 0, MarginRight = 6 };
			var percent = String.Format(" {0,3}%", Math.Round(percentage * 100));
			
			Console.CursorLeft = 0;
			//cpb.MarginRight = percent.Length + 1;
			Console.Write("{0,-15} ", truncate(command, 15));
			cpb.Draw(percentage);
			Console.Write(percent);
		}
		
		static string truncate(string value, int length) {
			return value ?? value.Substring(0, Math.Min(value.Length, length));
		}
	}
}