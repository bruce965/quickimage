﻿using System;
using QuickImage.Scripting.CSharp;

namespace QuickImage.Scripting
{
	public static class ScriptEngine
	{
		public static void Run(string code, ScriptingLanguage language) {
			switch (language) {
			case ScriptingLanguage.CSharp:
				CsEngine.Run(code);
				return;
			}
			
			throw new NotSupportedException("Unknown scripting language: " + language);
		}
	}
}
