﻿using System;
using System.CodeDom.Compiler;
using System.Globalization;
using System.Reflection;
using Microsoft.CSharp;

namespace QuickImage.Scripting.CSharp
{
	public static class CsEngine
	{
		public static void Run(string code) {
			var assembly = compileCsCode(code);
			if (assembly == null) {
				Console.Error.WriteLine("{0}", stringifyError("The script compilation failed"));
				return;
			}
			
			var type = assembly.GetType("Script");
			if (type == null) {
				Console.Error.WriteLine("{0}", stringifyError("The class 'Script' couldn't be found"));
				return;
			}
			
			var method = type.GetMethod("Run");
			if (method == null) {
				Console.Error.WriteLine("{0}", stringifyError("The method 'Run' coundn't be found in class 'Script'"));
				return;
			}
			
			// NOTE: static types are also abstract.
			if (!method.IsStatic && type.IsAbstract) {
				Console.Error.WriteLine("{0}", stringifyError("The method 'Run' in class 'Script' must be static"));
				return;
			}
			
			method.Invoke(
				method.IsStatic ? null : Activator.CreateInstance(type),
				null
			);
		}
		
		static Assembly compileCsCode(string code) {
			var cp = new CompilerParameters {
				GenerateExecutable = false,
				IncludeDebugInformation = true,
				GenerateInMemory = true
			};
			
			var assemblies = new[] {
				typeof(System.Uri).Assembly,  // System
				typeof(System.Action).Assembly,  // System.Core
				typeof(System.Drawing.Color).Assembly,  // System.Drawing
				typeof(QuickImage.Core.FColor).Assembly,  // QuickImage.Core
				typeof(QuickImage.Filters.Blur).Assembly,  // QuickImage.Filters
				typeof(QuickImage.Tools.ColorExtensionMethods).Assembly  // QuickImage.Tools
			};
			
			foreach (var assembly in assemblies)
				cp.ReferencedAssemblies.Add(assembly.Location);
			
			using (var cscp = new CSharpCodeProvider()) {
				var cr = cscp.CompileAssemblyFromSource(cp, new[] { code });
				
				foreach (CompilerError error in cr.Errors)
					Console.Error.WriteLine("{0}", stringifyError(error));
				
				try {
					return cr.CompiledAssembly;
				} catch(Exception) {
					return null;
				}
			}
		}
		
		static string stringifyError(string message, bool error = true, string code = null, int line = -1, int column = 0) {
			return String.Format(
				CultureInfo.InvariantCulture,
				"{0}{1}{2}: {3}",
				line == -1 ? "" : String.Format(CultureInfo.InvariantCulture, "({0},{1}) ", line, column),
				error ? "error" : "warning",
				String.IsNullOrEmpty(code) ? "" : (" " + code),
				message
			);
		}
		
		static string stringifyError(CompilerError error) {
			return stringifyError(error.ErrorText, !error.IsWarning, error.ErrorNumber, error.Line, error.Column);
		}
	}
}