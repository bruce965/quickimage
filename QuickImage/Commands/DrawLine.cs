﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using QuickImage.Core;
using QuickImage.Tools;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class DrawLine : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public Color Color = Color.Black;
		public float Size = 10;
		public float SX = 0;
		public float SY = 0;
		public float EX = 0;
		public float EY = 0;
		public bool Soft = false;
		
		public void Execute(ProgressListener onProgress) {
			// TODO: do not use `ToBitmap`.
			using (var bitmap = Source.ToBitmap())
			using (var graphics = Graphics.FromImage(bitmap))
			using (var pen = new Pen(Color, Size) { StartCap = LineCap.Round, EndCap = LineCap.Round }) {
				if (Soft)
					graphics.SmoothingMode = SmoothingMode.AntiAlias;
				
				graphics.DrawLine(pen, SX, SY, EX, EY);
			}
			
			Source.Save(Target ?? Source.Path);
		}
	}
}
