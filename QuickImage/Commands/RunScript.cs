﻿using System;
using System.IO;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Parsers;
using QuickImage.Scripting;

namespace QuickImage.Commands
{
	[Command]
	public class RunScript : ICommand
	{
		public string Script;
		public ScriptingLanguage Language = ScriptingLanguage.Unknown;
		
		public void Execute(ProgressListener onProgress) {
			ScriptEngine.Run(File.ReadAllText(Script), Language == ScriptingLanguage.Unknown ? ScriptingLanguageParser.Parse(Script) : Language);
		}
	}
}
