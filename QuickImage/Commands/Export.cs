﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using QuickImage.Core;
using QuickImage.Tools;
using QuickImage.Commands.Source;
using QuickImage.Parsers;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class Export : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public ImageFormat Format;
		
		public void Execute(ProgressListener onProgress) {
			Source.ExportFile(Target, Format ?? ImageFormatParser.Parse(Target));
		}
	}
}
