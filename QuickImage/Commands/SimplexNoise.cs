﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Core.OpenGL;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class SimplexNoise : ICommand
	{
		public FileRasterBuffer<Color> Target;
		public int Seed = 0;
		public int FrequencyX = 5;
		public int FrequencyY = 5;
		public float Amplitude = 1;
		public float Persistence = 0.5f;
		public float Time = 0;
		public int Octaves = 1;
		
		public void Execute(ProgressListener onProgress) {
			if (Renderer.IsSupported) {
				Filters.SimplexNoiseGPU.Run(
					Target,
					Seed,
					FrequencyX,
					FrequencyY,
					Amplitude,
					Persistence,
					Time,
					Octaves,
					true,
					true,
					onProgress
				);
			} else {
				Filters.SimplexNoise.Run(
					Target,
					Seed,
					FrequencyX,
					FrequencyY,
					Amplitude,
					Persistence,
					Time,
					Octaves,
					true,
					true,
					onProgress
				);
			}
			
			Target.Save();
		}
	}
}
