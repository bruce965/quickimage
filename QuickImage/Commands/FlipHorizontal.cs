﻿using System;
using System.Collections.Generic;
using QuickImage.Core;

namespace QuickImage.Commands
{
	public class FlipHorizontal : AbstractCommand
	{
		public FlipHorizontal() : base(1, 1) { }
		
		public override void Run(IDictionary<string, string> options, IList<string> args, Action<float> onProgress) {
			var inputPath = Argument(args, 0);
			var outputPath = Argument(args, 1, inputPath);
			
			var filter = new Filters.Flip {
				Horizontal = true
			};
			
			using (var buffer = RasterBuffer.FromFile(inputPath)) {
				filter.Run(buffer);
				buffer.Save(outputPath);
			}
			
			onProgress(1f);
		}
	}
}
