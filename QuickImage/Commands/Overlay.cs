﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class Overlay : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public RasterBuffer<Color> Image;
		public string Target;
		
		public void Execute(ProgressListener onProgress) {
			Filters.Overlay.Run(Source, Image, onProgress);
			Source.Save(Target ?? Source.Path);
		}
	}
}
