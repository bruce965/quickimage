﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Tools;
using QuickImage.Commands.Source;

namespace QuickImage.Commands
{
	[Command]
	public class Import : ICommand
	{
		public Image Source;
		public string Target;
		public int X;
		public int Y;
		
		public void Execute(ProgressListener onProgress) {
			var buffer = new RasterBuffer<Color>(Source.Size);
			buffer.ImportImage(Source);
			buffer.Save(Target);
		}
	}
}
