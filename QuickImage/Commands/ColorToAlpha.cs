﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class ColorToAlpha : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public Color Color;
		
		public void Execute(ProgressListener onProgress) {
			Filters.ColorToAlpha.Run(Source, Source, Color, onProgress);
			Source.Save(Target ?? Source.Path);
		}
	}
}
