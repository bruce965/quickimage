﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class WorleyNoise : ICommand
	{
		public FileRasterBuffer<Color> Target;
		
		public void Execute(ProgressListener onProgress) {
			Filters.WorleyNoise.Run(
				Target,
				onProgress
			);
			
			Target.Save();
		}
	}
}
