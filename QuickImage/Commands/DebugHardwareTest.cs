﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class DebugHardwareTest : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		
		public void Execute(ProgressListener onProgress) {
			Filters.DebugHardwareTest.Run(Source, onProgress);
			Source.Save(Target ?? Source.Path);
		}
	}
}
