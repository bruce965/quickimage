﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace QuickImage.Commands
{
	public class ArbitraryRotation : AbstractCommand
	{
		const string angle = "angle";
		const string centerx = "centerx";
		const string centery = "centery";
		
		public ArbitraryRotation() : base(1, 1, angle, centerx, centery) { }
		
		public override void Run(IDictionary<string, string> options, IList<string> args, Action<float> onProgress) {
			var inputPath = Argument(args, 0);
			var outputPath = Argument(args, 1, inputPath);
			
			var soft = Single.Parse(Option(options, ArbitraryRotation.angle, "0"));
			
			using (var output = OpenImageCopy(inputPath)) {
				var centerx = Single.Parse(Option(options, ArbitraryRotation.centerx, (output.Width / 2f).ToString()));
				var centery = Single.Parse(Option(options, ArbitraryRotation.centery, (output.Height / 2f).ToString()));
				
				using (var graphics = Graphics.FromImage(output)) {
					//graphics.DrawImage(TODO);
					throw new NotImplementedException();
				}
				
				//output.Save(outputPath, ImageFormat.MemoryBmp);
			}
		}
	}
}
