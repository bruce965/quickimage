﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class Flip : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public bool Horizontal = false;
		public bool Vertical = false;
		
		public void Execute(ProgressListener onProgress) {
			Filters.Flip.Run(Source, Horizontal, Vertical);
			Source.Save(Target ?? Source.Path);
		}
	}
}
