﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class Slur : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public int Seed = 0;
		public float Randomization = .5f;
		public int Repeat = 1;
		public bool HorizontallySeamless = false;
		public bool VerticallySeamless = false;
		
		public void Execute(ProgressListener onProgress) {
			Filters.Slur.Run(Source, Source, Seed, Randomization, Repeat, HorizontallySeamless, VerticallySeamless, onProgress);
			Source.Save(Target ?? Source.Path);
		}
	}
}
