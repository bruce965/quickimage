﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class Rotate : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public int Angle = 0;
		
		public void Execute(ProgressListener onProgress) {
			Filters.Rotate.Run(Source, Angle);
			Source.Save(Target ?? Source.Path);
		}
	}
}
