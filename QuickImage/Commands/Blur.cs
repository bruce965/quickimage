﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class Blur : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public int Radius = 5;
		public bool HorizontallySeamless = false;
		public bool VerticallySeamless = false;
		
		public void Execute(ProgressListener onProgress) {
			Filters.Blur.Run(Source, Source, Radius, HorizontallySeamless, VerticallySeamless, onProgress);
			Source.Save(Target ?? Source.Path);
		}
	}
}
