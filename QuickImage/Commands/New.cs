﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;

namespace QuickImage.Commands
{
	[Command]
	public class New : ICommand
	{
		public string Target;
		public int Width = 256;
		public int Height = 256;
		public int X = 0;
		public int Y = 0;
		public Color Color = Color.Transparent;
		
		public void Execute(ProgressListener onProgress) {
			var buffer = new RasterBuffer<Color>(Width, Height);
			buffer.X = X;
			buffer.Y = Y;
			
			Filters.Clear.Run(buffer, Color);
			
			buffer.Save(Target);
		}
	}
}
