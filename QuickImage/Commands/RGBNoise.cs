﻿using System;
using System.Drawing;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Types;

namespace QuickImage.Commands
{
	[Command]
	public class RGBNoise : ICommand
	{
		public FileRasterBuffer<Color> Source;
		public string Target;
		public int Seed = 0;
		public bool Correlated = false;
		public bool Independent = true;
		public float Red = 0f;
		public float Green = 0f;
		public float Blue = 0f;
		public float Alpha = 0f;
		
		public void Execute(ProgressListener onProgress) {
			Filters.RGBNoise.Run(Source, Seed, Correlated, Independent, Red, Green, Blue, Alpha);
			Source.Save(Target ?? Source.Path);
		}
	}
}
