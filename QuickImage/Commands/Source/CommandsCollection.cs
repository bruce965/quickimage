﻿using System;
using System.Collections;
using System.Collections.Generic;
using QuickImage.Commands.Source;

namespace QuickImage.Commands
{
	public class CommandsCollection : IEnumerable<CommandDefinition>
	{
		readonly Dictionary<string, CommandDefinition> commands = new Dictionary<string, CommandDefinition>();
		
		public CommandDefinition this[string name] {
			get {
				CommandDefinition command;
				if (!commands.TryGetValue(name.ToLowerInvariant(), out command))
					return null;
				
				return command;
			}
		}
		
		public void Add(CommandDefinition command) {
			commands.Add(command.Name.ToLowerInvariant(), command);
		}

		public IEnumerator<CommandDefinition> GetEnumerator() {
			return commands.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
}
