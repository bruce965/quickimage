﻿using System;
using System.Linq;
using System.Reflection;

namespace QuickImage.Commands.Source
{
	public static class CommandsFinder
	{
		public static CommandsCollection FindInAssemblies(params Assembly[] assemblies) {
			var commands = assemblies.SelectMany(assembly => assembly
				.GetTypes()
				.Where(type => type.IsDefined(typeof(CommandAttribute), true))
				.Where(type => typeof(ICommand).IsAssignableFrom(type))
			).Select(type => new CommandDefinition(type));
			
			var collection = new CommandsCollection();
			foreach (var command in commands)
				collection.Add(command);
			
			return collection;
		}
	}
}
