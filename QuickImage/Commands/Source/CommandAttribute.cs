﻿using System;

namespace QuickImage.Commands.Source
{
	[AttributeUsage(AttributeTargets.Class, Inherited=true, AllowMultiple=false)]
	public class CommandAttribute : Attribute { }
}
