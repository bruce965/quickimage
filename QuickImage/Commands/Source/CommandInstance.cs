﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuickImage.Core;
using QuickImage.Commands.Source;
using QuickImage.Parsers.Source;

namespace QuickImage.Commands
{
	public class CommandInstance
	{
		readonly string name;
		readonly ICommand instance;
		readonly TypeParsersCollection parser;
		readonly IDictionary<string, CommandDefinition.Parameter> parameters;
		
		public string Name {
			get { return name; }
		}
		
		public IEnumerable<CommandDefinition.Parameter> Parameters {
			get { return parameters.Values; }
		}
		
		public string this[string key] {
			get { return (parameters[key.ToLowerInvariant()].Getter(instance) ?? "").ToString(); }
			set {
				var param = parameters[key.ToLowerInvariant()];
				param.Setter(instance, parser.Parse(param.Type, value));
			}
		}
		
		public string this[int index] {
			get { return this[parameters.Keys.Skip(index).First()]; }
			set { this[parameters.Keys.Skip(index).First()] = value; }
		}
		
		public CommandInstance(CommandDefinition definition, TypeParsersCollection parsers) {
			name = definition.Name;
			instance = (ICommand)Activator.CreateInstance(definition.Type);
			parser = parsers;
			parameters = definition.Parameters.ToDictionary(parameter => parameter.Key.ToLowerInvariant());
		}
		
		public void Execute(ProgressListener onProgress) {
			instance.Execute(onProgress);
		}
	}
}
