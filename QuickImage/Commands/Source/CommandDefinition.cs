﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace QuickImage.Commands.Source
{
	public class CommandDefinition
	{
		public delegate object ParameterGetter(ICommand instance);
		public delegate void ParameterSetter(ICommand instance, object value);
		
		public class Parameter {
			public readonly string Key;
			public readonly Type Type;
			public readonly ParameterGetter Getter;
			public readonly ParameterSetter Setter;
			
			public Parameter(string key, Type type, ParameterGetter getter, ParameterSetter setter) {
				Key = key;
				Type = type;
				Getter = getter;
				Setter = setter;
			}
		}
		
		readonly Type type;
		
		public Type Type {
			get { return type; }
		}
		
		public string Name {
			get { return type.Name; }
		}
		
		public IEnumerable<Parameter> Parameters {
			get {
				return type
					.GetProperties(BindingFlags.Public | BindingFlags.Instance)
					.Select(property => Tuple.Create(property.PropertyType, (MemberInfo)property))
					.Concat(
						type
						.GetFields(BindingFlags.Public | BindingFlags.Instance)
						.Select(field => Tuple.Create(field.FieldType, (MemberInfo)field))
					)
					.Select(member => {
						var getterInstance = Expression.Parameter(typeof(ICommand));
						var setterInstance = Expression.Parameter(typeof(ICommand));
						var setterValue = Expression.Parameter(typeof(object));
						
						var param = new Parameter(
							member.Item2.Name,
							member.Item1,
							Expression.Lambda<ParameterGetter>(
								Expression.ConvertChecked(
									Expression.MakeMemberAccess(
										Expression.ConvertChecked(getterInstance, member.Item2.DeclaringType),
										member.Item2
									),
									typeof(object)
								),
								getterInstance
							).Compile(),
							Expression.Lambda<ParameterSetter>(
								Expression.Assign(
									Expression.MakeMemberAccess(
										Expression.ConvertChecked(setterInstance, member.Item2.DeclaringType),
										member.Item2
									),
									Expression.ConvertChecked(setterValue, member.Item1)
								),
								setterInstance,
								setterValue
							).Compile()
						);
						
						return param;
					});
			}
		}
		
		public CommandDefinition(Type type) {
			this.type = type;
		}
	}
}
