﻿using System;
using QuickImage.Core;

namespace QuickImage.Commands
{
	public interface ICommand
	{
		void Execute(ProgressListener onProgress);
	}
}
