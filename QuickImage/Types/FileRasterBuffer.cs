﻿using System;
using QuickImage.Core;

namespace QuickImage.Types
{
	public class FileRasterBuffer<T> : RasterBuffer<T>, ICloneable
	{
		public string Path { get; set; }
		
		FileRasterBuffer(string path, RasterBuffer<T> source) {
			Path = path;
			
			source.CopyTo(this);
		}
		
		public FileRasterBuffer(string path) : this(path, RasterBuffer<T>.FromFile(path)) { }
		
		public new FileRasterBuffer<T> Clone() {
			return new FileRasterBuffer<T>(Path, this);
		}
		
		public void Save() {
			base.Save(Path);
		}
		
		object ICloneable.Clone() {
			return Clone();
		}
	}
}
