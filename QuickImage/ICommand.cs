﻿using System;
using System.Collections.Generic;

namespace QuickImage
{
	public interface ICommand
	{
		IEnumerable<string> ParameterKeys { get; }
		int RequiredParametersCount { get; }
		int OptionalParametersCount { get; }
		
		void Run(IDictionary<string, string> dic, IList<string> list, Action<float> onProgress);
	}
}
