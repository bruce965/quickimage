﻿using System;
using System.Text;

namespace QuickImage
{
	public class ConsoleProgressBar
	{
		public string Start = "[";
		public string End = "]";
		public string Full = "=";
		public string Empty = " ";
		
		public int MarginLeft = 1;
		public int MarginRight = 1;
		
		public void Draw(float progress) {
			var top = Console.CursorTop;
			var left = Console.CursorLeft;
			
			var total = Console.BufferWidth - left - Start.Length - End.Length - MarginLeft - MarginRight;
			if (total <= 0)
				return;
			
			var full = (int)Math.Round(total * progress);
			var empty = total - full;
			
			Console.CursorLeft = left + MarginLeft;
			
			Console.Write(Start);
			Console.Write(repeatToFill(Full, full));
			Console.Write(repeatToFill(Empty, empty));
			Console.Write(End);
			Console.Out.Flush();
			
			//Console.CursorTop = top;
			//Console.CursorLeft = left;
		}
		
		static string repeatToFill(string what, int length) {
			if (what.Length == 0)
				return new String(' ', length);
			
			var sb = new StringBuilder();
			
			do {
				sb.Append(what);
			} while (sb.Length < length);
			
			sb.Length = length;
			return sb.ToString();
		}
	}
}
