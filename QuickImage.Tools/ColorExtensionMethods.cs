﻿using System;
using System.Drawing;
using QuickImage.Core;

namespace QuickImage.Tools
{
	public static class ColorExtensionMethods
	{
		public static FColor AlphaBlend(this FColor background, FColor foreground) {
			// https://en.wikipedia.org/wiki/Alpha_compositing#Alpha_blending
			
			if (background.Alpha == 0 && foreground.Alpha == 0)
				return new FColor();
			
			var osrca = 1f - foreground.Alpha;
			var dstaisrca = background.Alpha * osrca;
			var outa = foreground.Alpha + background.Alpha * osrca;
			var iouta = 1f / outa;
			
			return new FColor(
				outa,
				(foreground.Red * foreground.Alpha + background.Red * dstaisrca) * iouta,
				(foreground.Green * foreground.Alpha + background.Green * dstaisrca) * iouta,
				(foreground.Blue * foreground.Alpha + background.Blue * dstaisrca) * iouta
			);
		}
		
		public static Color AlphaBlend(this Color background, Color foreground) {
			return (Color)AlphaBlend((FColor)background, (FColor)foreground);
		}
	}
}
