﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using QuickImage.Core;

namespace QuickImage.Tools
{
	public static class ColorRasterBufferExtensionMethods
	{
		#region Import/Export, Conversion to/from Image
		
		/// <summary>Imports an <paramref name="image" /> to <paramref name="buffer" />.</summary>
		/// <param name="buffer">The <see cref="RasterBuffer&lt;Color&gt;" /> to import the image to.</param>
		/// <param name="image">The image to be imported.</param>
		public static void ImportImage(this RasterBuffer<Color> buffer, Image image) {
			buffer.Size = image.Size;
			//buffer.Offset = Point.Empty;
			
			var bitmap = image as Bitmap;
			bool shouldDisposeBitmap = false;
			if (bitmap == null) {
				bitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
				
				using (var graphics = Graphics.FromImage(bitmap))
					graphics.DrawImage(image, new Rectangle(Point.Empty, image.Size), new Rectangle(Point.Empty, image.Size), GraphicsUnit.Pixel);
				
				shouldDisposeBitmap = true;
			}
			
			for (var x = 0; x < buffer.Width; x++)
				for (var y = 0; y < buffer.Height; y++)
					buffer[x, y] = bitmap.GetPixel(x, y);
			
			if (shouldDisposeBitmap)
				bitmap.Dispose();
		}
		
		/// <summary>Creates an <see cref="Image" /> with <see cref="PixelFormat.Format32bppArgb" />.</summary>
		/// <param name="buffer">The <see cref="RasterBuffer&lt;Color&gt;" /> to create the image from.</param>
		public static Bitmap ToBitmap(this RasterBuffer<Color> buffer) {
			var bitmap = new Bitmap(buffer.Width, buffer.Height, PixelFormat.Format32bppArgb);
			
			for (var x = 0; x < buffer.Width; x++)
				for (var y = 0; y < buffer.Height; y++)
					bitmap.SetPixel(x, y, buffer[x, y]);
			
			return bitmap;
		}
		
		/// <summary>Imports an image from <paramref name="filename" />.</summary>
		/// <param name="buffer">The <see cref="RasterBuffer&lt;Color&gt;" /> to import the image to.</param>
		/// <param name="filename">The file which contains the image to be imported.</param>
		public static void ImportFile(this RasterBuffer<Color> buffer, string filename) {
			using (var image = Image.FromFile(filename))
				buffer.ImportImage(image);
		}
		
		/// <summary>Exports <paramref name="buffer" /> to file with the specified <paramref name="format" />.</summary>
		/// <param name="buffer">The <see cref="RasterBuffer&lt;Color&gt;" /> to export the image from.</param>
		/// <param name="filename">The file to export the image to.</param>
		/// <param name="format">The <see cref="ImageFormat" /> of the exported image.</param>
		public static void ExportFile(this RasterBuffer<Color> buffer, string filename, ImageFormat format) {
			using (var bitmap = buffer.ToBitmap())
				bitmap.Save(filename, format);
		}
		
		#endregion
		
		#region Editing
		
		// TODO: these methods should go in their own class.
		
		public static T GetPixelSeamless<T>(this RasterBuffer<T> buffer, int x, int y, bool hseamless, bool vseamless) {
			var width = buffer.Width;
			var height = buffer.Height;
			
			if (x < 0)
				x = hseamless ? (x % width + width) : 0;
			else if (x >= width)
				x = hseamless ? (x % width) : (width - 1);
			
			if (y < 0)
				y = vseamless ? (y % height + height) : 0;
			else if (y >= height)
				y = vseamless ? (y % height) : (height - 1);
			
			return buffer[x, y];
		}
		
		public static void SetPixelSeamless<T>(this RasterBuffer<T> buffer, T value, int x, int y, bool hseamless, bool vseamless) {
			var width = buffer.Width;
			var height = buffer.Height;
			
			if (x < 0)
				x = hseamless ? (x % width + width) : 0;
			else if (x >= width)
				x = hseamless ? (x % width) : (width - 1);
			
			if (y < 0)
				y = vseamless ? (y % height + height) : 0;
			else if (y >= height)
				y = vseamless ? (y % height) : (height - 1);
			
			buffer[x, y] = value;
		}
		
		#endregion
	}
}
