﻿using System;
using System.Drawing;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickImage.Core.OpenGL.Internal;
using QuickImage.Core.OpenGL.Internal.Shaders;

namespace QuickImage.Core.OpenGL
{
	// NOTE: this code is experimental, might break or be removed in the future
	// and might undergo heavy modifications at any time.
	
	public static class Renderer
	{
		const int size = 256;
		
		static readonly object isSupportedLock = new object();
		static bool? isSupported;
		
		public static bool IsSupported {
			get {
				lock (isSupportedLock) {
					if (isSupported.HasValue)
						return isSupported.Value;
					
					try {
						using (new Context())
							GL.Flush();  // any OpenGL command is fine
						isSupported = true;
					} catch(Exception) {
						isSupported = false;
					}
					
					return isSupported.Value;
				}
				
			}
		}
		
		public static void Render(RasterBuffer<Color> buffer, UniformsCollection uniforms, string code, ProgressListener onProgress) {
			using (new Context()) {
				int fbo, rboColor;
				init(out fbo, out rboColor);
				compute(buffer, uniforms, code, onProgress);
				dispose(fbo, rboColor);
			}
		}
		
		static void toBuffer(RasterBuffer<Color> buffer, byte[,,] pixels, int bx, int by, int w, int h) {
			GL.ReadPixels(0, 0, size, size, PixelFormat.Rgba, PixelType.UnsignedByte, pixels);
			
			for (var y = 0; y < h; y++) {
				var iy = size - 1 - y;
				
				for (var x = 0; x < w; x++) {
					var c = Color.FromArgb(pixels[iy, x, 3], pixels[iy, x, 0], pixels[iy, x, 1], pixels[iy, x, 2]);
					
					buffer[bx + x, by + y] = c;
				}
			}
		}
		
		static void compute(RasterBuffer<Color> buffer, IUniformsCollection uniformsCollection, string code, ProgressListener onProgress) {
			var uniforms = uniformsCollection.GetUniforms();
			
			var uniformsCode = new StringBuilder();
			foreach (var uniform in uniforms)
				uniformsCode.AppendFormat("uniform {0} {1};\n", uniform.Value.GLSLName, uniform.Key);
			
			var vertShaderSource = Shaders.VertShader;
			var vertShader = GL.CreateShader(ShaderType.VertexShader);
			GL.ShaderSource(vertShader, vertShaderSource);
			GL.CompileShader(vertShader);
			
			var info = GL.GetShaderInfoLog(vertShader);
			int statusCode;
			
			GL.GetShader(vertShader, ShaderParameter.CompileStatus, out statusCode);
			if (statusCode != 1)
				throw new GraphicsException(String.Format("ERROR:\n{0}\nCODE:\n{1}", info, vertShaderSource));
			
			var fragShaderSource = Shaders.FragShader.Replace("/*{UNIFORMS_HERE}*/", uniformsCode.ToString()).Replace("/*{CODE_HERE}*/", code);
			var fragShader = GL.CreateShader(ShaderType.FragmentShader);
			GL.ShaderSource(fragShader, fragShaderSource);
			GL.CompileShader(fragShader);
			
			info = GL.GetShaderInfoLog(fragShader);
			GL.GetShader(fragShader, ShaderParameter.CompileStatus, out statusCode);
			if (statusCode != 1)
				throw new GraphicsException(String.Format("ERROR:\n{0}\nCODE:\n{1}", info, fragShaderSource));
			
			var program = GL.CreateProgram();
			GL.AttachShader(program, vertShader);
			GL.AttachShader(program, fragShader);
			
			GL.LinkProgram(program);
			info = GL.GetProgramInfoLog(program);
			GL.GetProgram(program, GetProgramParameterName.LinkStatus, out statusCode);
			if (statusCode != 1)
				throw new GraphicsException(info);
			
			GL.DeleteShader(vertShader);
			GL.DeleteShader(fragShader);
			
			GL.UseProgram(program);
			
			var _qim_Offset = GL.GetUniformLocation(program, "_qim_Offset");
			
			foreach (var uniform in uniforms) {
				var location = GL.GetUniformLocation(program, uniform.Key);
				uniform.Value.AssignTo(location);
			}
			
			var vertArray = GL.GenVertexArray();
			GL.BindVertexArray(vertArray);
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertArray);
			var __qim_Position = GL.GetAttribLocation(program, "_qim_PositionVert");
			GL.VertexAttribPointer(__qim_Position, 2, VertexAttribPointerType.Float, true, sizeof(float) * 2, 0);
			
			// TODO: do not render off-buffer pixels to save some computations
			GL.BufferData(BufferTarget.ArrayBuffer, sizeof(float) * 2 * 6, new[] { -1f, -1f, -1f, 1f, 1f, 1f, -1f, -1f, 1f, 1f, 1f, -1f }, BufferUsageHint.StaticDraw);
			
			GL.EnableVertexAttribArray(__qim_Position);
			
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertArray);
			GL.VertexAttribPointer(0, 6, VertexAttribPointerType.Float, false, 0, 0);
			
			var pixels = new byte[size, size, 4];
			
			var startX = buffer.X;
			var startY = buffer.Y;
			var endX = startX + buffer.Width;
			var endY = startY + buffer.Height;
			
			var chunksCount = (int)(Math.Ceiling((float)(endY - startY) / size) * Math.Ceiling((float)(endX - startX) / size));
			var currentChunk = 0;
			for (var y = startY; y < endY; y += size) {
				for (var x = startX; x < endX; x += size) {
					GL.Uniform2(_qim_Offset, x, y);
					GL.DrawArrays(PrimitiveType.Triangles, 0, 6);
					
					GL.Flush();
					toBuffer(buffer, pixels, x - startX, y - startY, Math.Min(size, endX - x), Math.Min(size, endY - y));
					
					currentChunk++;
					
					if (onProgress != null)
						onProgress((float)currentChunk / chunksCount);
				}
			}
			
			GL.DisableVertexAttribArray(__qim_Position);
			
			GL.DeleteVertexArray(vertArray);
			
			GL.DeleteProgram(program);
		}
		
		static void init(out int fbo, out int rboColor) {
			// Framebuffer
			fbo = GL.GenFramebuffer();
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
			
			// Color renderbuffer
			rboColor = GL.GenRenderbuffer();
			GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, rboColor);
			GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.Rgba8, size, size);
			GL.FramebufferRenderbuffer(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.ColorAttachment0, RenderbufferTarget.Renderbuffer, rboColor);
			
			GL.ReadBuffer(ReadBufferMode.ColorAttachment0);
			
			// Sanity check
			var fbs = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
			if (fbs != FramebufferErrorCode.FramebufferComplete) {
				dispose(fbo, rboColor);
				throw new GraphicsException("Framebuffer Status: " + fbs);
			}
			
			GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
			GL.Viewport(0, 0, size, size);
			
			GL.ClearColor(Color.Transparent);
			GL.Clear(ClearBufferMask.ColorBufferBit);
		}
		
		static void dispose(int fbo, int rboColor) {
			GL.DeleteFramebuffer(fbo);
			GL.DeleteRenderbuffer(rboColor);
		}
	}
}
