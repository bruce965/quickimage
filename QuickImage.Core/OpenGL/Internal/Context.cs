﻿using System;
using OpenTK;
using OpenTK.Graphics;

namespace QuickImage.Core.OpenGL.Internal
{
	class Context : IDisposable
	{
		readonly GameWindow window;
		
		public Context() {
			// TODO: a window shouldn't be necessary.
			window = new GameWindow(
				1, 1,
				GraphicsMode.Default,
				String.Empty,
				GameWindowFlags.FixedWindow,
				DisplayDevice.Default,
				3, 0,
				GraphicsContextFlags.Offscreen
			);
			
			window.Closed += (sender, e) => window.Dispose();
		}

		public void Dispose() {
			window.Dispose();
		}
	}
}
