﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK.Graphics.OpenGL;
using QuickImage.Core.OpenGL.GLSL;

namespace QuickImage.Core.OpenGL.Internal
{
	static class UniformAssigners
	{
		readonly static IDictionary<Type, IUniformAssigner> types = new Dictionary<Type, IUniformAssigner> {
			{ typeof(float), new UniformAssigner<float>("float", (value, location) => GL.Uniform1(location, value), (array, location) => GL.Uniform1(location, array.Length, array)) },
			{ typeof(bool), new UniformAssigner<bool>("bool", (value, location) => GL.Uniform1(location, value ? 1 : 0), (array, location) => GL.Uniform1(location, array.Length, array.Select(value => value ? 1 : 0).ToArray())) },
			{ typeof(int), new UniformAssigner<int>("int", (value, location) => GL.Uniform1(location, value), (array, location) => GL.Uniform1(location, array.Length, array)) },
			{ typeof(uint), new UniformAssigner<uint>("uint", (value, location) => GL.Uniform1(location, value), (array, location) => GL.Uniform1(location, array.Length, array)) },
			{ typeof(vec2), new UniformAssigner<vec2>("vec2", (value, location) => GL.Uniform2(location, value.x, value.y), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y }).ToArray())) },
			{ typeof(vec3), new UniformAssigner<vec3>("vec3", (value, location) => GL.Uniform3(location, value.x, value.y, value.z), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y, value.z }).ToArray())) },
			{ typeof(vec4), new UniformAssigner<vec4>("vec4", (value, location) => GL.Uniform4(location, value.x, value.y, value.z, value.w), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y, value.z, value.w }).ToArray())) },
			//{ typeof(bvec2), new UniformAssigner<bvec2>("bvec2", (value, location) => GL.Uniform2(location, value.x ? 1 : 0, value.y ? 1 : 0), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x ? 1 : 0, value.y ? 1 : 0 }).ToArray())) },
			//{ typeof(bvec3), new UniformAssigner<bvec3>("bvec3", (value, location) => GL.Uniform3(location, value.x ? 1 : 0, value.y ? 1 : 0, value.z ? 1 : 0), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x ? 1 : 0, value.y ? 1 : 0, value.z ? 1 : 0 }).ToArray())) },
			//{ typeof(bvec4), new UniformAssigner<bvec4>("bvec4", (value, location) => GL.Uniform4(location, value.x ? 1 : 0, value.y ? 1 : 0, value.z ? 1 : 0, value.w ? 1 : 0), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x ? 1 : 0, value.y ? 1 : 0, value.z ? 1 : 0, value.w ? 1 : 0 }).ToArray())) },
			//{ typeof(ivec2), new UniformAssigner<ivec2>("ivec2", (value, location) => GL.Uniform2(location, value.x, value.y), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y }).ToArray())) },
			//{ typeof(ivec3), new UniformAssigner<ivec3>("ivec3", (value, location) => GL.Uniform3(location, value.x, value.y, value.z), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y, value.z }).ToArray())) },
			//{ typeof(ivec4), new UniformAssigner<ivec4>("ivec4", (value, location) => GL.Uniform4(location, value.x, value.y, value.z, value.w), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y, value.z, value.w }).ToArray())) },
			//{ typeof(uvec2), new UniformAssigner<uvec2>("uvec2", (value, location) => GL.Uniform2(location, value.x, value.y), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y }).ToArray())) },
			//{ typeof(uvec3), new UniformAssigner<uvec3>("uvec3", (value, location) => GL.Uniform3(location, value.x, value.y, value.z), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y, value.z }).ToArray())) },
			//{ typeof(uvec4), new UniformAssigner<uvec4>("uvec4", (value, location) => GL.Uniform4(location, value.x, value.y, value.z, value.w), (array, location) => GL.Uniform4(location, array.Length, array.SelectMany(value => new[] { value.x, value.y, value.z, value.w }).ToArray())) },
			//{ typeof(mat2), new UniformAssigner<mat2>("mat2", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat3), new UniformAssigner<mat3>("mat3", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat4), new UniformAssigner<mat4>("mat4", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat2x2), new UniformAssigner<mat2x2>("mat2x2", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat2x3), new UniformAssigner<mat2x3>("mat2x3", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat2x4), new UniformAssigner<mat2x4>("mat2x4", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat3x2), new UniformAssigner<mat3x2>("mat3x2", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat3x3), new UniformAssigner<mat3x3>("mat3x3", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat3x4), new UniformAssigner<mat3x4>("mat3x4", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat4x2), new UniformAssigner<mat4x2>("mat4x2", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat4x3), new UniformAssigner<mat4x3>("mat4x3", (value, location) => TODO, (array, location) => TODO) },
			//{ typeof(mat4x4), new UniformAssigner<mat4x4>("mat4x4", (value, location) => TODO, (array, location) => TODO) }
		};
		
		public static IUniformAssigner ByNative(Type type) {
			IUniformAssigner assigner;
			if (!types.TryGetValue(type, out assigner))
				throw new ArgumentException(String.Format("GLSL doesn't support uniforms of type \"{0}\"", type.FullName));
			
			return assigner;
		}
		
		public static IUniformAssigner ByNative<T>() {
			var type = typeof(T);
			return ByNative(type.GetElementType() ?? type);
		}
	}
}
