﻿using System;
using System.Collections.Generic;

namespace QuickImage.Core.OpenGL.Internal
{
	interface IUniformsCollection
	{
		IDictionary<string, IUniformBinding> GetUniforms();
	}
}
