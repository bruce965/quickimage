﻿using System;
using System.IO;

namespace QuickImage.Core.OpenGL.Internal.Shaders
{
	static class Shaders
	{
		public static string FragShader { get; private set; }
		public static string VertShader { get; private set; }
		
		static Shaders() {
			FragShader = readShader("FragShader.glsl");
			VertShader = readShader("VertShader.glsl");
		}
		
		static string readShader(string name) {
			using (var stream = typeof(Renderer).Assembly.GetManifestResourceStream(typeof(Shaders), name))
			using (var streamReader = new StreamReader(stream))
				return streamReader.ReadToEnd();
		}
	}
}
