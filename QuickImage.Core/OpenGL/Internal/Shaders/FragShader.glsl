﻿#version 130

/*{UNIFORMS_HERE}*/

uniform ivec2 _qim_Offset;

in vec2 _qim_PositionFrag;

vec4 compute(int x, int y);

void main() {
	gl_FragColor = compute(int(_qim_PositionFrag.x * 256 + _qim_Offset.x), int(256 - _qim_PositionFrag.y * 256 + _qim_Offset.y));
}

/*{CODE_HERE}*/
