﻿#version 130

in vec2 _qim_PositionVert;

out vec2 _qim_PositionFrag;

void main() {
	gl_Position = vec4(_qim_PositionVert, 0, 1);
	_qim_PositionFrag = vec2((_qim_PositionVert.x + 1) * 0.5, (_qim_PositionVert.y + 1) * 0.5);
}
