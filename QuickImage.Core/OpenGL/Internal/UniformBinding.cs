﻿using System;

namespace QuickImage.Core.OpenGL.Internal
{
	interface IUniformBinding
	{
		string GLSLName { get; }
		
		void AssignTo(int location);
	}
	
	interface IUniformBinding<T> : IUniformBinding { }
	
	class UniformBinding<T> : IUniformBinding<T>, IUniformBinding
	{
		readonly T value;
		readonly UniformAssigner<T> assigner;

		public string GLSLName {
			get { return assigner.GLSLName; }
		}
		
		public UniformBinding(T value, UniformAssigner<T> assigner) {
			this.value = value;
			this.assigner = assigner;
		}
		
		public void AssignTo(int location) {
			assigner.Assign(value, location);
		}
	}
	
	class UniformArrayBinding<T> : IUniformBinding<T[]>, IUniformBinding {
		
		readonly T[] value;
		readonly UniformAssigner<T> assigner;

		public string GLSLName {
			get { return String.Format("{0}[{1}]", assigner.GLSLName, value.Length); }
		}
		
		public UniformArrayBinding(T[] value, UniformAssigner<T> assigner) {
			this.value = value;
			this.assigner = assigner;
		}
		
		public void AssignTo(int location) {
			assigner.Assign(value, location);
		}
	}
}
