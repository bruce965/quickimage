﻿using System;

namespace QuickImage.Core.OpenGL.Internal
{
	interface IUniformAssigner
	{
		string GLSLName { get; }
		
		IUniformBinding Bind(object value);
		void Assign(object value, int location);
	}
	
	class UniformAssigner<T> : IUniformAssigner
	{
		readonly string name;
		readonly Action<T, int> assign;
		readonly Action<T[], int> assignArray;
		
		public string GLSLName {
			get { return name; }
		}
		
		public UniformAssigner(string name, Action<T, int> assign, Action<T[], int> assignArray) {
			this.name = name;
			this.assign = assign;
			this.assignArray = assignArray;
		}
		
		public IUniformBinding<T[]> Bind(T[] value) {
			return new UniformArrayBinding<T>(value, this);
		}
		
		public IUniformBinding<T> Bind(T value) {
			return new UniformBinding<T>(value, this);
		}
		
		public void Assign(T value, int location) {
			assign(value, location);
		}
		
		public void Assign(T[] value, int location) {
			assignArray(value, location);
		}
		
		IUniformBinding IUniformAssigner.Bind(object value) {
			return value.GetType().IsArray ? (IUniformBinding)Bind((T[])value) : (IUniformBinding)Bind((T)value);
		}
		
		void IUniformAssigner.Assign(object value, int location) {
			var valueArray = value as T[];
			if (valueArray != null)  // TODO: flatten multi-dimensional arrays
				Assign(valueArray, location);
			else
				Assign((T)value, location);
		}
	}
}
