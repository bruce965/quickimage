﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using QuickImage.Core.OpenGL.Internal;

namespace QuickImage.Core.OpenGL
{
	/// <summary>Contains .NET bindings for GLSL uniforms.</summary>
	public class UniformsCollection : IUniformsCollection, IEnumerable
	{
		readonly IDictionary<string, IUniformBinding> uniforms = new Dictionary<string, IUniformBinding>();
		
		/// <summary>Add a uniform to this collection.</summary>
		/// <param name="key">Name of the uniform in GLSL.</param>
		/// <param name="value">Value of the uniform in .NET.</param>
		public void Add<T>(string key, T value) {
			uniforms.Add(key, UniformAssigners.ByNative<T>().Bind(value));
		}

		IDictionary<string, IUniformBinding> IUniformsCollection.GetUniforms() {
			return uniforms;
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return uniforms.GetEnumerator();
		}
	}
}
