﻿using System;

namespace QuickImage.Core.OpenGL.GLSL
{
	public struct vec3
	{
		float _x, _y, _z;
		
		public float x { get { return _x; } set { _x = value; } }
		public float y { get { return _y; } set { _y = value; } }
		public float z { get { return _z; } set { _z = value; } }
		
		public float r { get { return _x; } set { _x = value; } }
		public float g { get { return _y; } set { _y = value; } }
		public float b { get { return _z; } set { _z = value; } }
		
		public float s { get { return _x; } set { _x = value; } }
		public float t { get { return _y; } set { _y = value; } }
		public float p { get { return _z; } set { _z = value; } }
		
		public vec2 xx { get { return new vec2(x, x); } }
		public vec2 xy { get { return new vec2(x, y); } }
		public vec2 xz { get { return new vec2(x, z); } }
		public vec2 yx { get { return new vec2(y, x); } }
		public vec2 yy { get { return new vec2(y, y); } }
		public vec2 yz { get { return new vec2(y, z); } }
		public vec2 zx { get { return new vec2(z, x); } }
		public vec2 zy { get { return new vec2(z, y); } }
		public vec2 zz { get { return new vec2(z, z); } }
		
		public vec2 rr { get { return new vec2(r, r); } }
		public vec2 rg { get { return new vec2(r, g); } }
		public vec2 rb { get { return new vec2(r, b); } }
		public vec2 gr { get { return new vec2(g, r); } }
		public vec2 gg { get { return new vec2(g, g); } }
		public vec2 gb { get { return new vec2(g, b); } }
		public vec2 br { get { return new vec2(b, r); } }
		public vec2 bg { get { return new vec2(b, g); } }
		public vec2 bb { get { return new vec2(b, b); } }
		
		public vec2 ss { get { return new vec2(s, s); } }
		public vec2 st { get { return new vec2(s, t); } }
		public vec2 sp { get { return new vec2(s, p); } }
		public vec2 ts { get { return new vec2(t, s); } }
		public vec2 tt { get { return new vec2(t, t); } }
		public vec2 tp { get { return new vec2(t, p); } }
		public vec2 ps { get { return new vec2(p, s); } }
		public vec2 pt { get { return new vec2(p, t); } }
		public vec2 pp { get { return new vec2(p, p); } }
		
		public vec3 xxx { get { return new vec3(x, x, x); } }
		public vec3 xxy { get { return new vec3(x, x, y); } }
		public vec3 xxz { get { return new vec3(x, x, z); } }
		public vec3 xyx { get { return new vec3(x, y, x); } }
		public vec3 xyy { get { return new vec3(x, y, y); } }
		public vec3 xyz { get { return new vec3(x, y, z); } }
		public vec3 xzx { get { return new vec3(x, z, x); } }
		public vec3 xzy { get { return new vec3(x, z, y); } }
		public vec3 xzz { get { return new vec3(x, z, z); } }
		public vec3 yxx { get { return new vec3(y, x, x); } }
		public vec3 yxy { get { return new vec3(y, x, y); } }
		public vec3 yxz { get { return new vec3(y, x, z); } }
		public vec3 yyx { get { return new vec3(y, y, x); } }
		public vec3 yyy { get { return new vec3(y, y, y); } }
		public vec3 yyz { get { return new vec3(y, y, z); } }
		public vec3 yzx { get { return new vec3(y, z, x); } }
		public vec3 yzy { get { return new vec3(y, z, y); } }
		public vec3 yzz { get { return new vec3(y, z, z); } }
		public vec3 zxx { get { return new vec3(z, x, x); } }
		public vec3 zxy { get { return new vec3(z, x, y); } }
		public vec3 zxz { get { return new vec3(z, x, z); } }
		public vec3 zyx { get { return new vec3(z, y, x); } }
		public vec3 zyy { get { return new vec3(z, y, y); } }
		public vec3 zyz { get { return new vec3(z, y, z); } }
		public vec3 zzx { get { return new vec3(z, z, x); } }
		public vec3 zzy { get { return new vec3(z, z, y); } }
		public vec3 zzz { get { return new vec3(z, z, z); } }
		
		public vec3 rrr { get { return new vec3(r, r, r); } }
		public vec3 rrg { get { return new vec3(r, r, g); } }
		public vec3 rrb { get { return new vec3(r, r, b); } }
		public vec3 rgr { get { return new vec3(r, g, r); } }
		public vec3 rgg { get { return new vec3(r, g, g); } }
		public vec3 rgb { get { return new vec3(r, g, b); } }
		public vec3 rbr { get { return new vec3(r, b, r); } }
		public vec3 rbg { get { return new vec3(r, b, g); } }
		public vec3 rbb { get { return new vec3(r, b, b); } }
		public vec3 grr { get { return new vec3(g, r, r); } }
		public vec3 grg { get { return new vec3(g, r, g); } }
		public vec3 grb { get { return new vec3(g, r, b); } }
		public vec3 ggr { get { return new vec3(g, g, r); } }
		public vec3 ggg { get { return new vec3(g, g, g); } }
		public vec3 ggb { get { return new vec3(g, g, b); } }
		public vec3 gbr { get { return new vec3(g, b, r); } }
		public vec3 gbg { get { return new vec3(g, b, g); } }
		public vec3 gbb { get { return new vec3(g, b, b); } }
		public vec3 brr { get { return new vec3(b, r, r); } }
		public vec3 brg { get { return new vec3(b, r, g); } }
		public vec3 brb { get { return new vec3(b, r, b); } }
		public vec3 bgr { get { return new vec3(b, g, r); } }
		public vec3 bgg { get { return new vec3(b, g, g); } }
		public vec3 bgb { get { return new vec3(b, g, b); } }
		public vec3 bbr { get { return new vec3(b, b, r); } }
		public vec3 bbg { get { return new vec3(b, b, g); } }
		public vec3 bbb { get { return new vec3(b, b, b); } }
		
		public vec3 sss { get { return new vec3(s, s, s); } }
		public vec3 sst { get { return new vec3(s, s, t); } }
		public vec3 ssp { get { return new vec3(s, s, p); } }
		public vec3 sts { get { return new vec3(s, t, s); } }
		public vec3 stt { get { return new vec3(s, t, t); } }
		public vec3 stp { get { return new vec3(s, t, p); } }
		public vec3 sps { get { return new vec3(s, p, s); } }
		public vec3 spt { get { return new vec3(s, p, t); } }
		public vec3 spp { get { return new vec3(s, p, p); } }
		public vec3 tss { get { return new vec3(t, s, s); } }
		public vec3 tst { get { return new vec3(t, s, t); } }
		public vec3 tsp { get { return new vec3(t, s, p); } }
		public vec3 tts { get { return new vec3(t, t, s); } }
		public vec3 ttt { get { return new vec3(t, t, t); } }
		public vec3 ttp { get { return new vec3(t, t, p); } }
		public vec3 tps { get { return new vec3(t, p, s); } }
		public vec3 tpt { get { return new vec3(t, p, t); } }
		public vec3 tpp { get { return new vec3(t, p, p); } }
		public vec3 pss { get { return new vec3(p, s, s); } }
		public vec3 pst { get { return new vec3(p, s, t); } }
		public vec3 psp { get { return new vec3(p, s, p); } }
		public vec3 pts { get { return new vec3(p, t, s); } }
		public vec3 ptt { get { return new vec3(p, t, t); } }
		public vec3 ptp { get { return new vec3(p, t, p); } }
		public vec3 pps { get { return new vec3(p, p, s); } }
		public vec3 ppt { get { return new vec3(p, p, t); } }
		public vec3 ppp { get { return new vec3(p, p, p); } }
		
		public float this[int i] {
			get {
				switch (i) {
					case 0: return _x;
					case 1: return _y;
					case 2: return _z;
				}
				return 0;
			}
			set {
				switch (i) {
					case 0: _x = value; break;
					case 1: _y = value; break;
					case 2: _z = value; break;
				}
			}
		}
		
		public vec3(float x, float y, float z) {
			_x = x;
			_y = y;
			_z = z;
		}
		
		public vec3(vec2 v, float z) : this(v.x, v.y, z) { }
		
		public static implicit operator vec2(vec3 v) {
			return v.xy;
		}
	}
}
