﻿using System;

namespace QuickImage.Core.OpenGL.GLSL
{
	public struct vec2
	{
		float _x, _y;
		
		public float x { get { return _x; } set { _x = value; } }
		public float y { get { return _y; } set { _y = value; } }
		
		public float r { get { return _x; } set { _x = value; } }
		public float g { get { return _y; } set { _y = value; } }
		
		public float s { get { return _x; } set { _x = value; } }
		public float t { get { return _y; } set { _y = value; } }
		
		public vec2 xx { get { return new vec2(x, x); } }
		public vec2 xy { get { return new vec2(x, y); } }
		public vec2 yx { get { return new vec2(y, x); } }
		public vec2 yy { get { return new vec2(y, y); } }
		
		public vec2 rr { get { return new vec2(r, r); } }
		public vec2 rg { get { return new vec2(r, g); } }
		public vec2 gr { get { return new vec2(g, r); } }
		public vec2 gg { get { return new vec2(g, g); } }
		
		public vec2 ss { get { return new vec2(s, s); } }
		public vec2 st { get { return new vec2(s, t); } }
		public vec2 ts { get { return new vec2(t, s); } }
		public vec2 tt { get { return new vec2(t, t); } }
		
		public float this[int i] {
			get {
				switch (i) {
					case 0: return _x;
					case 1: return _y;
				}
				return 0;
			}
			set {
				switch (i) {
					case 0: _x = value; break;
					case 1: _y = value; break;
				}
			}
		}
		
		public vec2(float x, float y) {
			_x = x;
			_y = y;
		}
	}
}
