﻿using System;

namespace QuickImage.Core.OpenGL.GLSL
{
	public struct vec4
	{
		float _x, _y, _z, _w;
		
		public float x { get { return _x; } set { _x = value; } }
		public float y { get { return _y; } set { _y = value; } }
		public float z { get { return _z; } set { _z = value; } }
		public float w { get { return _w; } set { _w = value; } }
		
		public float r { get { return _x; } set { _x = value; } }
		public float g { get { return _y; } set { _y = value; } }
		public float b { get { return _z; } set { _z = value; } }
		public float a { get { return _w; } set { _w = value; } }
		
		public float s { get { return _x; } set { _x = value; } }
		public float t { get { return _y; } set { _y = value; } }
		public float p { get { return _z; } set { _z = value; } }
		public float q { get { return _w; } set { _w = value; } }
		
		public vec2 xx { get { return new vec2(x, x); } }
		public vec2 xy { get { return new vec2(x, y); } }
		public vec2 xz { get { return new vec2(x, z); } }
		public vec2 xw { get { return new vec2(x, w); } }
		public vec2 yx { get { return new vec2(y, x); } }
		public vec2 yy { get { return new vec2(y, y); } }
		public vec2 yz { get { return new vec2(y, z); } }
		public vec2 yw { get { return new vec2(y, w); } }
		public vec2 zx { get { return new vec2(z, x); } }
		public vec2 zy { get { return new vec2(z, y); } }
		public vec2 zz { get { return new vec2(z, z); } }
		public vec2 zw { get { return new vec2(z, w); } }
		
		public vec2 rr { get { return new vec2(r, r); } }
		public vec2 rg { get { return new vec2(r, g); } }
		public vec2 rb { get { return new vec2(r, b); } }
		public vec2 ra { get { return new vec2(r, a); } }
		public vec2 gr { get { return new vec2(g, r); } }
		public vec2 gg { get { return new vec2(g, g); } }
		public vec2 gb { get { return new vec2(g, b); } }
		public vec2 ga { get { return new vec2(g, a); } }
		public vec2 br { get { return new vec2(b, r); } }
		public vec2 bg { get { return new vec2(b, g); } }
		public vec2 bb { get { return new vec2(b, b); } }
		public vec2 ba { get { return new vec2(b, a); } }
		
		public vec2 ss { get { return new vec2(s, s); } }
		public vec2 st { get { return new vec2(s, t); } }
		public vec2 sp { get { return new vec2(s, p); } }
		public vec2 sq { get { return new vec2(s, q); } }
		public vec2 ts { get { return new vec2(t, s); } }
		public vec2 tt { get { return new vec2(t, t); } }
		public vec2 tp { get { return new vec2(t, p); } }
		public vec2 tq { get { return new vec2(t, q); } }
		public vec2 ps { get { return new vec2(p, s); } }
		public vec2 pt { get { return new vec2(p, t); } }
		public vec2 pp { get { return new vec2(p, p); } }
		public vec2 pq { get { return new vec2(p, q); } }
		
		public vec3 xxx { get { return new vec3(x, x, x); } }
		public vec3 xxy { get { return new vec3(x, x, y); } }
		public vec3 xxz { get { return new vec3(x, x, z); } }
		public vec3 xxw { get { return new vec3(x, x, w); } }
		public vec3 xyx { get { return new vec3(x, y, x); } }
		public vec3 xyy { get { return new vec3(x, y, y); } }
		public vec3 xyz { get { return new vec3(x, y, z); } }
		public vec3 xyw { get { return new vec3(x, y, w); } }
		public vec3 xzx { get { return new vec3(x, z, x); } }
		public vec3 xzy { get { return new vec3(x, z, y); } }
		public vec3 xzz { get { return new vec3(x, z, z); } }
		public vec3 xzw { get { return new vec3(x, z, w); } }
		public vec3 xwx { get { return new vec3(x, w, x); } }
		public vec3 xwy { get { return new vec3(x, w, y); } }
		public vec3 xwz { get { return new vec3(x, w, z); } }
		public vec3 xww { get { return new vec3(x, w, w); } }
		public vec3 yxx { get { return new vec3(y, x, x); } }
		public vec3 yxy { get { return new vec3(y, x, y); } }
		public vec3 yxz { get { return new vec3(y, x, z); } }
		public vec3 yxw { get { return new vec3(y, x, w); } }
		public vec3 yyx { get { return new vec3(y, y, x); } }
		public vec3 yyy { get { return new vec3(y, y, y); } }
		public vec3 yyz { get { return new vec3(y, y, z); } }
		public vec3 yyw { get { return new vec3(y, y, w); } }
		public vec3 yzx { get { return new vec3(y, z, x); } }
		public vec3 yzy { get { return new vec3(y, z, y); } }
		public vec3 yzz { get { return new vec3(y, z, z); } }
		public vec3 yzw { get { return new vec3(y, z, w); } }
		public vec3 ywx { get { return new vec3(y, w, x); } }
		public vec3 ywy { get { return new vec3(y, w, y); } }
		public vec3 ywz { get { return new vec3(y, w, z); } }
		public vec3 yww { get { return new vec3(y, w, w); } }
		public vec3 zxx { get { return new vec3(z, x, x); } }
		public vec3 zxy { get { return new vec3(z, x, y); } }
		public vec3 zxz { get { return new vec3(z, x, z); } }
		public vec3 zxw { get { return new vec3(z, x, w); } }
		public vec3 zyx { get { return new vec3(z, y, x); } }
		public vec3 zyy { get { return new vec3(z, y, y); } }
		public vec3 zyz { get { return new vec3(z, y, z); } }
		public vec3 zyw { get { return new vec3(z, y, w); } }
		public vec3 zzx { get { return new vec3(z, z, x); } }
		public vec3 zzy { get { return new vec3(z, z, y); } }
		public vec3 zzz { get { return new vec3(z, z, z); } }
		public vec3 zzw { get { return new vec3(z, z, w); } }
		public vec3 zwx { get { return new vec3(z, w, x); } }
		public vec3 zwy { get { return new vec3(z, w, y); } }
		public vec3 zwz { get { return new vec3(z, w, z); } }
		public vec3 zww { get { return new vec3(z, w, w); } }
		public vec3 wxx { get { return new vec3(w, x, x); } }
		public vec3 wxy { get { return new vec3(w, x, y); } }
		public vec3 wxz { get { return new vec3(w, x, z); } }
		public vec3 wxw { get { return new vec3(w, x, w); } }
		public vec3 wyx { get { return new vec3(w, y, x); } }
		public vec3 wyy { get { return new vec3(w, y, y); } }
		public vec3 wyz { get { return new vec3(w, y, z); } }
		public vec3 wyw { get { return new vec3(w, y, w); } }
		public vec3 wzx { get { return new vec3(w, z, x); } }
		public vec3 wzy { get { return new vec3(w, z, y); } }
		public vec3 wzz { get { return new vec3(w, z, z); } }
		public vec3 wzw { get { return new vec3(w, z, w); } }
		public vec3 wwx { get { return new vec3(w, w, x); } }
		public vec3 wwy { get { return new vec3(w, w, y); } }
		public vec3 wwz { get { return new vec3(w, w, z); } }
		public vec3 www { get { return new vec3(w, w, w); } }
		
		public vec3 rrr { get { return new vec3(r, r, r); } }
		public vec3 rrg { get { return new vec3(r, r, g); } }
		public vec3 rrb { get { return new vec3(r, r, b); } }
		public vec3 rra { get { return new vec3(r, r, a); } }
		public vec3 rgr { get { return new vec3(r, g, r); } }
		public vec3 rgg { get { return new vec3(r, g, g); } }
		public vec3 rgb { get { return new vec3(r, g, b); } }
		public vec3 rga { get { return new vec3(r, g, a); } }
		public vec3 rbr { get { return new vec3(r, b, r); } }
		public vec3 rbg { get { return new vec3(r, b, g); } }
		public vec3 rbb { get { return new vec3(r, b, b); } }
		public vec3 rba { get { return new vec3(r, b, a); } }
		public vec3 rar { get { return new vec3(r, a, r); } }
		public vec3 rag { get { return new vec3(r, a, g); } }
		public vec3 rab { get { return new vec3(r, a, b); } }
		public vec3 raa { get { return new vec3(r, a, a); } }
		public vec3 grr { get { return new vec3(g, r, r); } }
		public vec3 grg { get { return new vec3(g, r, g); } }
		public vec3 grb { get { return new vec3(g, r, b); } }
		public vec3 gra { get { return new vec3(g, r, a); } }
		public vec3 ggr { get { return new vec3(g, g, r); } }
		public vec3 ggg { get { return new vec3(g, g, g); } }
		public vec3 ggb { get { return new vec3(g, g, b); } }
		public vec3 gga { get { return new vec3(g, g, a); } }
		public vec3 gbr { get { return new vec3(g, b, r); } }
		public vec3 gbg { get { return new vec3(g, b, g); } }
		public vec3 gbb { get { return new vec3(g, b, b); } }
		public vec3 gba { get { return new vec3(g, b, a); } }
		public vec3 gar { get { return new vec3(g, a, r); } }
		public vec3 gag { get { return new vec3(g, a, g); } }
		public vec3 gab { get { return new vec3(g, a, b); } }
		public vec3 gaa { get { return new vec3(g, a, a); } }
		public vec3 brr { get { return new vec3(b, r, r); } }
		public vec3 brg { get { return new vec3(b, r, g); } }
		public vec3 brb { get { return new vec3(b, r, b); } }
		public vec3 bra { get { return new vec3(b, r, a); } }
		public vec3 bgr { get { return new vec3(b, g, r); } }
		public vec3 bgg { get { return new vec3(b, g, g); } }
		public vec3 bgb { get { return new vec3(b, g, b); } }
		public vec3 bga { get { return new vec3(b, g, a); } }
		public vec3 bbr { get { return new vec3(b, b, r); } }
		public vec3 bbg { get { return new vec3(b, b, g); } }
		public vec3 bbb { get { return new vec3(b, b, b); } }
		public vec3 bba { get { return new vec3(b, b, a); } }
		public vec3 bar { get { return new vec3(b, a, r); } }
		public vec3 bag { get { return new vec3(b, a, g); } }
		public vec3 bab { get { return new vec3(b, a, b); } }
		public vec3 baa { get { return new vec3(b, a, a); } }
		public vec3 arr { get { return new vec3(a, r, r); } }
		public vec3 arg { get { return new vec3(a, r, g); } }
		public vec3 arb { get { return new vec3(a, r, b); } }
		public vec3 ara { get { return new vec3(a, r, a); } }
		public vec3 agr { get { return new vec3(a, g, r); } }
		public vec3 agg { get { return new vec3(a, g, g); } }
		public vec3 agb { get { return new vec3(a, g, b); } }
		public vec3 aga { get { return new vec3(a, g, a); } }
		public vec3 abr { get { return new vec3(a, b, r); } }
		public vec3 abg { get { return new vec3(a, b, g); } }
		public vec3 abb { get { return new vec3(a, b, b); } }
		public vec3 aba { get { return new vec3(a, b, a); } }
		public vec3 aar { get { return new vec3(a, a, r); } }
		public vec3 aag { get { return new vec3(a, a, g); } }
		public vec3 aab { get { return new vec3(a, a, b); } }
		public vec3 aaa { get { return new vec3(a, a, a); } }
		
		public vec3 sss { get { return new vec3(s, s, s); } }
		public vec3 sst { get { return new vec3(s, s, t); } }
		public vec3 ssp { get { return new vec3(s, s, p); } }
		public vec3 ssq { get { return new vec3(s, s, q); } }
		public vec3 sts { get { return new vec3(s, t, s); } }
		public vec3 stt { get { return new vec3(s, t, t); } }
		public vec3 stp { get { return new vec3(s, t, p); } }
		public vec3 stq { get { return new vec3(s, t, q); } }
		public vec3 sps { get { return new vec3(s, p, s); } }
		public vec3 spt { get { return new vec3(s, p, t); } }
		public vec3 spp { get { return new vec3(s, p, p); } }
		public vec3 spq { get { return new vec3(s, p, q); } }
		public vec3 sqs { get { return new vec3(s, q, s); } }
		public vec3 sqt { get { return new vec3(s, q, t); } }
		public vec3 sqp { get { return new vec3(s, q, p); } }
		public vec3 sqq { get { return new vec3(s, q, q); } }
		public vec3 tss { get { return new vec3(t, s, s); } }
		public vec3 tst { get { return new vec3(t, s, t); } }
		public vec3 tsp { get { return new vec3(t, s, p); } }
		public vec3 tsq { get { return new vec3(t, s, q); } }
		public vec3 tts { get { return new vec3(t, t, s); } }
		public vec3 ttt { get { return new vec3(t, t, t); } }
		public vec3 ttp { get { return new vec3(t, t, p); } }
		public vec3 ttq { get { return new vec3(t, t, q); } }
		public vec3 tps { get { return new vec3(t, p, s); } }
		public vec3 tpt { get { return new vec3(t, p, t); } }
		public vec3 tpp { get { return new vec3(t, p, p); } }
		public vec3 tpq { get { return new vec3(t, p, q); } }
		public vec3 tqs { get { return new vec3(t, q, s); } }
		public vec3 tqt { get { return new vec3(t, q, t); } }
		public vec3 tqp { get { return new vec3(t, q, p); } }
		public vec3 tqq { get { return new vec3(t, q, q); } }
		public vec3 pss { get { return new vec3(p, s, s); } }
		public vec3 pst { get { return new vec3(p, s, t); } }
		public vec3 psp { get { return new vec3(p, s, p); } }
		public vec3 psq { get { return new vec3(p, s, q); } }
		public vec3 pts { get { return new vec3(p, t, s); } }
		public vec3 ptt { get { return new vec3(p, t, t); } }
		public vec3 ptp { get { return new vec3(p, t, p); } }
		public vec3 ptq { get { return new vec3(p, t, q); } }
		public vec3 pps { get { return new vec3(p, p, s); } }
		public vec3 ppt { get { return new vec3(p, p, t); } }
		public vec3 ppp { get { return new vec3(p, p, p); } }
		public vec3 ppq { get { return new vec3(p, p, q); } }
		public vec3 pqs { get { return new vec3(p, q, s); } }
		public vec3 pqt { get { return new vec3(p, q, t); } }
		public vec3 pqp { get { return new vec3(p, q, p); } }
		public vec3 pqq { get { return new vec3(p, q, q); } }
		public vec3 qss { get { return new vec3(q, s, s); } }
		public vec3 qst { get { return new vec3(q, s, t); } }
		public vec3 qsp { get { return new vec3(q, s, p); } }
		public vec3 qsq { get { return new vec3(q, s, q); } }
		public vec3 qts { get { return new vec3(q, t, s); } }
		public vec3 qtt { get { return new vec3(q, t, t); } }
		public vec3 qtp { get { return new vec3(q, t, p); } }
		public vec3 qtq { get { return new vec3(q, t, q); } }
		public vec3 qps { get { return new vec3(q, p, s); } }
		public vec3 qpt { get { return new vec3(q, p, t); } }
		public vec3 qpp { get { return new vec3(q, p, p); } }
		public vec3 qpq { get { return new vec3(q, p, q); } }
		public vec3 qqs { get { return new vec3(q, q, s); } }
		public vec3 qqt { get { return new vec3(q, q, t); } }
		public vec3 qqp { get { return new vec3(q, q, p); } }
		public vec3 qqq { get { return new vec3(q, q, q); } }
		
		public float this[int i] {
			get {
				switch (i) {
					case 0: return _x;
					case 1: return _y;
					case 2: return _z;
					case 3: return _w;
				}
				return 0;
			}
			set {
				switch (i) {
					case 0: _x = value; break;
					case 1: _y = value; break;
					case 2: _z = value; break;
					case 3: _w = value; break;
				}
			}
		}
		
		public vec4(float x, float y, float z, float w) {
			_x = x;
			_y = y;
			_z = z;
			_w = w;
		}
		
		public vec4(vec2 v, float z, float w) : this(v.x, v.y, z, w) { }
		
		public vec4(vec3 v, float w) : this(v.x, v.y, v.z, w) { }
		
		public static implicit operator vec2(vec4 v) {
			return v.xy;
		}
		
		public static implicit operator vec3(vec4 v) {
			return v.xyz;
		}
	}
}
