﻿using System;

namespace QuickImage.Core.Internal
{
	abstract class AbstractDisposable : IDisposable {
		
		readonly object lockable = new object();
		bool disposed;
		
		protected bool Disposed {
			get { lock (lockable) { return disposed; } }
			set { lock (lockable) { disposed = value; } }
		}
		
		protected abstract void Dispose(bool disposing);
		
		void IDisposable.Dispose() {
			dispose(true);
		}
		
		void dispose(bool disposing) {
			lock (lockable) {
				if (disposed)
					return;
				
				Dispose(true);
				
				disposed = true;
				GC.SuppressFinalize(this);
			}
		}
		
		~AbstractDisposable() {
			dispose(false);
		}
	}
}
