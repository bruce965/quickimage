﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QuickImage.Core.Internal
{
	class EnumerableCollection<T> : ICollection<T>
	{
		readonly IEnumerable<T> super;
		
		public EnumerableCollection(IEnumerable<T> super) {
			this.super = super;
		}
		
		public void Add(T item) {
			throw new NotSupportedException();
		}
		
		public void Clear() {
			throw new NotSupportedException();
		}
		
		public bool Contains(T item) {
			return super.Contains(item);
		}
		
		public void CopyTo(T[] array, int arrayIndex) {
			if (array == null)
				throw new ArgumentNullException();
			
			if (arrayIndex < 0)
				throw new IndexOutOfRangeException();
			
			if (super.Count() > array.Length - arrayIndex)
				throw new ArgumentException();
			
			foreach (var item in super)
				array[arrayIndex++] = item;
		}
		
		public bool Remove(T item) {
			throw new NotSupportedException();
		}
		
		public int Count {
			get { return super.Count(); }
		}
		
		public bool IsReadOnly {
			get { return true; }
		}
		
		public IEnumerator<T> GetEnumerator() {
			return super.GetEnumerator();
		}
		
		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
	
	static class EnumerableCollectionExtensionMethods {
		
		public static ICollection<T> ToReadOnlyCollection<T>(this IEnumerable<T> enumerable) {
			return new EnumerableCollection<T>(enumerable);
		}
	}
}
