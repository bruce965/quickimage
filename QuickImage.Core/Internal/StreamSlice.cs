﻿using System;
using System.IO;

namespace QuickImage.Core.Internal
{
	static class StreamExtensionMethods
	{
		class StreamSlice : Stream
		{
			readonly Stream source;
			readonly long start;
			
			public StreamSlice(Stream source) : this(source, source.Position) { }
			
			public StreamSlice(Stream source, long start) {
				this.source = source;
				this.start = start;
			}
			
			public override void Flush() {
				source.Flush();
			}
			
			public override long Seek(long offset, SeekOrigin origin) {
				return source.Seek(offset + (origin == SeekOrigin.Begin ? start : 0), origin);
			}
			
			public override void SetLength(long value) {
				source.SetLength(value + start);
			}
			
			public override int Read(byte[] buffer, int offset, int count) {
				return source.Read(buffer, offset, count);
			}
			
			public override void Write(byte[] buffer, int offset, int count) {
				source.Write(buffer, offset, count);
			}
			
			public override bool CanRead {
				get { return source.CanRead; }
			}
			
			public override bool CanSeek {
				get { return source.CanSeek; }
			}
			
			public override bool CanWrite {
				get { return source.CanWrite; }
			}
			
			public override long Length {
				get { return source.Length - start; }
			}
			
			public override long Position {
				get { return source.Position - start; }
				set { source.Position = value + start; }
			}
		}
		
		public static Stream Slice(this Stream stream) {
			return new StreamSlice(stream, stream.Position);
		}
	}
}
