﻿using System;

namespace QuickImage.Core.Internal
{
	public static class ArrayExtensionMethods
	{
		public static void CopyTo<T>(this T[,] source, T[,] target) {
			var width = Math.Min(source.GetLength(0), target.GetLength(0));
			var height = Math.Min(source.GetLength(1), target.GetLength(1));
			
			for (var x = 0; x < width; x++)
				for (var y = 0; y < height; y++)
					target[x, y] = source[x, y];
		}
	}
}
