﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QuickImage.Core.Internal
{
	class TypedAccessor<TKey, TValue, TSuperValue> : IDictionary<TKey, TValue> where TValue : TSuperValue {
		
		readonly IDictionary<TKey, TSuperValue> super;
		
		public TypedAccessor(IDictionary<TKey, TSuperValue> super) {
			this.super = super;
		}
		
		public bool ContainsKey(TKey key) {
			TSuperValue value;
			return super.TryGetValue(key, out value) && value is TValue;
		}

		public void Add(TKey key, TValue value) {
			super.Add(key, value);
		}

		public bool Remove(TKey key) {
			TSuperValue value;
			if (!super.TryGetValue(key, out value) || !(value is TValue))
				return false;
			
			return super.Remove(key);
		}

		public bool TryGetValue(TKey key, out TValue value) {
			TSuperValue superValue;
			if (!super.TryGetValue(key, out superValue) || !(superValue is TValue)) {
				value = default(TValue);
				return false;
			}
			
			value = (TValue)superValue;
			return true;
		}

		public TValue this[TKey key] {
			get {
				var value = super[key];
				if (value is TValue)
					return (TValue)value;
				
				throw new KeyNotFoundException();
			}
			set {
				super[key] = value;
			}
		}

		public ICollection<TKey> Keys {
			get { return super.Where(kvp => kvp.Value is TValue).Select(kvp => kvp.Key).ToReadOnlyCollection(); }
		}

		public ICollection<TValue> Values {
			get { return super.Select(kvp => kvp.Value).Where(value => value is TValue).Cast<TValue>().ToReadOnlyCollection(); }
		}

		public void Add(KeyValuePair<TKey, TValue> item) {
			super.Add(new KeyValuePair<TKey, TSuperValue>(item.Key, item.Value));
		}

		public void Clear() {
			super.Clear();
		}

		public bool Contains(KeyValuePair<TKey, TValue> item) {
			return super.Contains(new KeyValuePair<TKey, TSuperValue>(item.Key, item.Value));
		}

		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) {
			if (array == null)
				throw new ArgumentNullException();
			
			if (arrayIndex < 0)
				throw new IndexOutOfRangeException();
			
			var items = super.Where(kvp => kvp.Value is TValue).Select(kvp => new KeyValuePair<TKey, TValue>(kvp.Key, (TValue)kvp.Value));
			
			if (items.Count() > array.Length - arrayIndex)
				throw new ArgumentException();
			
			foreach (var item in items)
				array[arrayIndex++] = item;
		}

		public bool Remove(KeyValuePair<TKey, TValue> item) {
			return super.Remove(new KeyValuePair<TKey, TSuperValue>(item.Key, item.Value));
		}

		public int Count {
			get { return super.Values.Count(value => value is TValue); }
		}

		public bool IsReadOnly {
			get { return super.IsReadOnly; }
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
			return super.Where(kvp => kvp.Value is TValue).Select(kvp => new KeyValuePair<TKey, TValue>(kvp.Key, (TValue)kvp.Value)).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
}
