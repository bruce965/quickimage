﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using QuickImage.Core.Internal;

namespace QuickImage.Core
{
	/// <summary>Buffer containing a bidimensional array of data.</summary>
	public class RasterBuffer<T> : ICloneable
	{
		#region Offset
		
		Point offset;
		
		/// <summary>Horizontal offset from parent container's left edge in pixels.</summary>
		public int X {
			get { return offset.X; }
			set { offset.X = value; }
		}
		
		/// <summary>Vertical offset from parent container's top edge in pixels.</summary>
		public int Y {
			get { return offset.Y; }
			set { offset.Y = value; }
		}
		
		/// <summary>Offset from parent container's top-left edge in pixels.</summary>
		public Point Offset {
			get { return offset; }
			set { offset = value; }
		}
		
		#endregion
		
		#region Dimensions
		
		/// <summary>Width in pixels.</summary>
		public int Width {
			get { return data.GetLength(0); }
			set { resize(value, Height); }
		}
		
		/// <summary>Height in pixels.</summary>
		public int Height {
			get { return data.GetLength(1); }
			set { resize(Width, value); }
		}
		
		/// <summary>Size in pixels.</summary>
		public Size Size {
			get { return new Size(data.GetLength(0), data.GetLength(1)); }
			set { resize(value.Width, value.Height); }
		}
		
		public RasterBuffer(Size size) {
			data = new T[size.Width, size.Height];
		}
		
		public RasterBuffer(int width, int height) : this(new Size(width, height)) { }
		
		public RasterBuffer() : this(256, 256) { }
		
		RasterBuffer(T[,] data) {
			this.data = data;
		}
		
		void resize(int width, int height) {
			if (width == Width && height == Height)
				return;
			
			var oldData = data;
			
			data = new T[width, height];
			oldData.CopyTo(data);
		}
		
		#endregion
		
		#region Data
		
		T[,] data;
		
		public T this[int x, int y] {
			get { return data[x, y]; }
			set { data[x, y] = value; }
		}
		
		/// <summary>Create a new copy of this buffer.</summary>
		public RasterBuffer<T> Clone() {
			return new RasterBuffer<T>((T[,])data.Clone()) {
				offset = offset
			};
		}

		object ICloneable.Clone() {
			return Clone();
		}
		
		/// <summary>Copy data from this buffer to another, resizing target if necessary.</summary>
		public void CopyTo(RasterBuffer<T> target) {
			target.resize(Width, Height);
			target.offset = offset;
			
			data.CopyTo(target.data);
		}
		
		#endregion
		
		#region Serialization
		
		#region Types
		
		// disable StaticFieldInGenericType
		static bool isRegistered;
		static long typeMagicNumber;
		static Action<BinaryWriter, T> serialize;
		static Func<BinaryReader, T> deserialize;
		// restore StaticFieldInGenericType
		
		public static void RegisterSerializer(long magicNumber, Action<BinaryWriter, T> serializer, Func<BinaryReader, T> deserializer) {
			if (isRegistered)
				throw new InvalidOperationException(String.Format("Serializer/Deserializer already registered for '{0}'", typeof(T).FullName));
			
			RasterBufferTypes.reserveMagicNumber(magicNumber);
			
			isRegistered = true;
			typeMagicNumber = magicNumber;
			serialize = serializer;
			deserialize = deserializer;
		}
		
		static RasterBuffer() {
			RasterBufferTypes.initialize();
		}
		
		#endregion
		
		const long MagicNumber = 361889941841;
		const ushort Version = 1;
		
		/// <summary>Creates a buffer from the specified data stream.</summary>
		public static RasterBuffer<T> FromStream(Stream stream) {
			if (!isRegistered)
				throw new InvalidOperationException(String.Format("Unable to deserialize '{0}', register the type before loading", typeof(T).FullName));
			
			using (var reader = new BinaryReader(stream, Encoding.UTF8)) {
				if (reader.ReadInt64() != MagicNumber)
					throw new FormatException("Invalid magic number");
				
				if (reader.ReadUInt16() != Version)
					throw new FormatException("Unsupported RasterBuffer version");
				
				if (reader.ReadInt64() != typeMagicNumber)
					throw new FormatException("Mismatching RasterBuffer type");
				
				var offsetX = reader.ReadInt32();
				var offsetY = reader.ReadInt32();
				
				var width = reader.ReadInt32();
				var height = reader.ReadInt32();
				
				var gzip = stream;
				//using (var gzip = new GZipStream(stream, CompressionMode.Decompress))
				using (var reader2 = new BinaryReader(gzip, Encoding.UTF8)) {
					var buffer = new RasterBuffer<T>(width, height) {
						offset = new Point(offsetX, offsetY)
					};
					
					for (var x = 0; x < width; x++)
						for (var y = 0; y < height; y++)
							buffer.data[x, y] = deserialize(reader2);
					
					return buffer;
				}
			}
		}
		
		/// <summary>Saves this buffer to the specified data stream.</summary>
		public void Save(Stream stream) {
			if (!isRegistered)
				throw new InvalidOperationException(String.Format("Unable to serialize '{0}', register the type before saving", typeof(T).FullName));
			
			var writer = new BinaryWriter(stream, Encoding.UTF8);
			writer.Write(MagicNumber);
			writer.Write(Version);
			writer.Write(typeMagicNumber);
			writer.Write(offset.X);
			writer.Write(offset.Y);
			
			var width = data.GetLength(0);
			var height = data.GetLength(1);
			
			writer.Write(width);
			writer.Write(height);
			writer.Flush();
			
			var gzip = stream;
			//var gzip = new GZipStream(stream, CompressionMode.Compress);
			var writer2 = new BinaryWriter(gzip, Encoding.UTF8);
			
			for (var x = 0; x < width; x++)
				for (var y = 0; y < height; y++)
					serialize(writer2, data[x, y]);
			
			writer2.Flush();
			gzip.Flush();
		}
		
		/// <summary>Creates a buffer from the specified file.</summary>
		public static RasterBuffer<T> FromFile(string filename) {
			using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
				return FromStream(stream);
		}
		
		/// <summary>Saves this buffer to the specified file.</summary>
		public void Save(string filename) {
			using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
				Save(stream);
		}
		
		#endregion
	}
	
	static class RasterBufferTypes {
		
		static bool initialized;
		
		static readonly HashSet<long> magicNumbers = new HashSet<long>();
		
		// This is a trick to allow serialization/deserialization of known types.
		internal static void initialize() {
			if (initialized)
				return;
			
			initialized = true;
			
			RasterBuffer<Boolean>.RegisterSerializer(-1, (writer, value) => writer.Write(value), reader => reader.ReadBoolean());
			RasterBuffer<Byte>.RegisterSerializer(-2, (writer, value) => writer.Write(value), reader => reader.ReadByte());
			RasterBuffer<SByte>.RegisterSerializer(-3, (writer, value) => writer.Write(value), reader => reader.ReadSByte());
			RasterBuffer<Char>.RegisterSerializer(-4, (writer, value) => writer.Write(value), reader => reader.ReadChar());
			RasterBuffer<Decimal>.RegisterSerializer(-5, (writer, value) => writer.Write(value), reader => reader.ReadDecimal());
			RasterBuffer<Double>.RegisterSerializer(-6, (writer, value) => writer.Write(value), reader => reader.ReadDouble());
			RasterBuffer<Single>.RegisterSerializer(-7, (writer, value) => writer.Write(value), reader => reader.ReadSingle());
			RasterBuffer<Int32>.RegisterSerializer(-8, (writer, value) => writer.Write(value), reader => reader.ReadInt32());
			RasterBuffer<UInt32>.RegisterSerializer(-9, (writer, value) => writer.Write(value), reader => reader.ReadUInt32());
			RasterBuffer<Int64>.RegisterSerializer(-10, (writer, value) => writer.Write(value), reader => reader.ReadInt64());
			RasterBuffer<UInt64>.RegisterSerializer(-11, (writer, value) => writer.Write(value), reader => reader.ReadUInt64());
			RasterBuffer<Int16>.RegisterSerializer(-12, (writer, value) => writer.Write(value), reader => reader.ReadInt16());
			RasterBuffer<UInt16>.RegisterSerializer(-13, (writer, value) => writer.Write(value), reader => reader.ReadUInt16());
			
			RasterBuffer<String>.RegisterSerializer(-100, (writer, value) => writer.Write(value), reader => reader.ReadString());
			RasterBuffer<Color>.RegisterSerializer(-101, (writer, value) => writer.Write(value.ToArgb()), reader => Color.FromArgb(reader.ReadInt32()));
		}
		
		internal static void reserveMagicNumber(long magicNumber) {
			if (!magicNumbers.Add(magicNumber))
				throw new InvalidOperationException("Magic number already registered: " + magicNumber);
		}
	}
}
