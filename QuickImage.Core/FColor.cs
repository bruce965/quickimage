﻿using System;
using System.Drawing;
using System.Text;

namespace QuickImage.Core
{
	public struct FColor
	{
		const float i255 = 1f / 255f;
		
		float a, r, g, b;
		
		public float Alpha { get { return a; } set { a = value; } }
		public float Red { get { return r; } set { r = value; } }
		public float Green { get { return g; } set { g = value; } }
		public float Blue { get { return b; } set { b = value; } }
		
		public FColor(float alpha, float red, float green, float blue) {
			a = alpha;
			r = red;
			g = green;
			b = blue;
		}
		
		public FColor(float red, float green, float blue) : this(1f, red, green, blue) { }
		
		public static implicit operator Color(FColor color) {
			return Color.FromArgb(
				(int)(color.a * 255f),
				(int)(color.r * 255f),
				(int)(color.g * 255f),
				(int)(color.b * 255f)
			);
		}
		
		public static explicit operator FColor(Color color) {
			return new FColor {
				a = color.A * i255,
				r = color.R * i255,
				g = color.G * i255,
				b = color.B * i255
			};
		}
		
		public override string ToString() {
			return String.Format("FColor [A={0}, R={1}, G={2}, B={3}]", a, r, g, b);
		}
	}
}
